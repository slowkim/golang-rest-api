package dto

import (
	"time"
)

type MemberSummary struct {
	Id                 int64                  `json:"id"`
	Nickname           string                 `json:"nickname"`
	ProfileImageURL    string                 `json:"profileImageURL"`
	Mobile             string                 `json:"mobile"`
	Email              string                 `json:"email"`
	MobilizerId        int64                  `json:"mobilizerId"`
	DistributorId      int64                  `json:"distributorId"`
	TotalDonationCount int64                  `json:"totalDonationCount"`
	RecentlyDonation   MemberRecentlyDonation `json:"recentlyDonation"`
	CreatedAt          time.Time              `json:"createdAt"`
	UpdatedAt          time.Time              `json:"updatedAt"`
}

type MemberRecentlyDonation struct {
	Id                 int64  `json:"id"`
	DonorName          string `json:"donorName"`
	DonorMobile        string `json:"donorMobile"`
	DonorPostNo        string `json:"donorPostNo"`
	DonorAddress       string `json:"donorAddress"`
	DonorAddressDetail string `json:"donorAddressDetail"`
}
