package dto

type PageResult struct {
	Result     interface{} `json:"result"`
	TotalCount int64       `json:"totalCount"`
}

type ApiError struct {
	Code    int         `json:"code"`
	Details interface{} `json:"details"`
	Message string      `json:"message"`
}
