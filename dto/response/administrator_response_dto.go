package dto

import "time"

type AdministratorSummary struct {
	Id        int64     `json:"id"`
	Email     string    `json:"email" `
	Mobile    string    `json:"mobile" `
	Roles     []string  `json:"roles"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
}
