package dto

type AddressWithPosition struct {
	PlaceName   string `json:"placeName"`
	AddressName string `json:"addressName"`
	Phone       string `json:"phone"`
	Latitude    string `json:"latitude"`  // a.k.a Y
	Longitude   string `json:"longitude"` //a.k.a X
}

type Address struct {
	AddressName     string `json:"addressName"`
	LoadAddressName string `json:"loadAddressName"`
	LoadZoneNo      string `json:"loadZoneNo"`
}
