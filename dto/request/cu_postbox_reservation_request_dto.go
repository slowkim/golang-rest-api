package dto

type CUPostReservation struct {
	ComGb        string `json:"com_gb"`
	CustOrdNo    string `json:"cust_ord_no"`
	AcptDt       string `json:"acpt_dt"`
	SendTel      string `json:"send_tel"`
	SendName     string `json:"send_name"`
	SendZip      string `json:"send_zip"`
	SendAddr1    string `json:"send_addr1"`
	SendAddr2    string `json:"send_addr2"`
	GoodsNo      string `json:"goods_no"`
	Goods        string `json:"goods"`
	GoodsCnt     int64  `json:"goods_cnt"`
	Extra        string `json:"extra"`
	GoodsPrice   int64  `json:"goods_price"`
	ReceiveName  string `json:"receive_name"`
	ReceiveTel   string `json:"receive_tel"`
	ReceiveZip   string `json:"receive_zip"`
	ReceiveAddr1 string `json:"receive_addr1"`
	ReceiveAddr2 string `json:"receive_addr2"`
	TLSGB        int64  `json:"tls_gb"`
	GoodsInfo    string `json:"goods_info"`
	OrderNum     string `json:"order_num"`
	LimitWgt     int64  `json:"limit_wgt"`
	CancelFlag   string `json:"cancel_flag"`
	DcAmt        int64  `json:"dc_amt"`
	SmsYn        string `json:"sms_yn"`
}
