package dto

import (
	"golang-rest-api/config"

	"github.com/labstack/echo/v4"
)

var (
	echoApp          *echo.Echo
	handleWithFilter func(handlerFunc echo.HandlerFunc, c echo.Context) error
)

func init() {
	e := config.ConfigureEcho()
	echoApp = e
	handleWithFilter = func(handlerFunc echo.HandlerFunc, c echo.Context) error {
		return (handlerFunc)(c)
	}
}
