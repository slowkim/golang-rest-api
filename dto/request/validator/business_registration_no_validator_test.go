package validator

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValidateBusinessRegistrationNo(t *testing.T) {
	t.Run("사업자등록번호 체크", func(t *testing.T) {
		// given
		businessRegistrationNo := "2018121534"

		// when
		result := validateBusinessRegistrationNo(businessRegistrationNo)

		// then
		assert.True(t, result)
	})

	t.Run("사업자등록번호 체크 - 공백(required와 함께 쓸것)", func(t *testing.T) {
		// given
		businessRegistrationNo := ""

		// when
		result := validateBusinessRegistrationNo(businessRegistrationNo)

		// then
		assert.True(t, result)
	})

	t.Run("사업자등록번호 체크 - 문자 포함", func(t *testing.T) {
		// given
		businessRegistrationNo := "201812157A"

		// when
		result := validateBusinessRegistrationNo(businessRegistrationNo)

		// then
		assert.False(t, result)
	})

	t.Run("사업자등록번호 체크 - 10자리아닌경우", func(t *testing.T) {
		// given
		businessRegistrationNo := "123456789"

		// when
		result := validateBusinessRegistrationNo(businessRegistrationNo)

		// then
		assert.False(t, result)
	})

	t.Run("사업자등록번호 체크 - 유효한번호아닌경우", func(t *testing.T) {
		// given
		businessRegistrationNo := "1234567890"

		// when
		result := validateBusinessRegistrationNo(businessRegistrationNo)

		// then
		assert.False(t, result)
	})
}
