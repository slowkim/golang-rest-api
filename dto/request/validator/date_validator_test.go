package validator

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValidateDate(t *testing.T) {
	t.Run("8자리 날짜 체크", func(t *testing.T) {
		// given
		date := "20210301"

		// when
		result := validateDate8(date)

		// then
		assert.True(t, result)
	})

	t.Run("8자리 날짜 체크 - 공백(required와 함께 쓸것)", func(t *testing.T) {
		// given
		date := ""

		// when
		result := validateDate8(date)

		// then
		assert.True(t, result)
	})

	t.Run("8자리 날짜 체크 - 문자 포함", func(t *testing.T) {
		// given
		date := "2018121A"

		// when
		result := validateDate8(date)

		// then
		assert.False(t, result)
	})

	t.Run("8자리 날짜 체크 - yyyyMMdd 날짜형식이 아닌경우", func(t *testing.T) {
		// given
		date := "20210351"

		// when
		result := validateDate8(date)

		// then
		assert.False(t, result)
	})
}

func TestValidateDatePeriod(t *testing.T) {
	t.Run("6자리 앞뒤 날짜 체크", func(t *testing.T) {
		// given
		startDate := "20210301"
		endDate := "20210302"

		// when
		err := ValidateDatePeriod(startDate, endDate)

		// then
		assert.Equal(t, nil, err)
	})

	t.Run("6자리 앞뒤 날짜 체크 - 시작일이 yyyyMMdd 날짜형식이 아닌경우", func(t *testing.T) {
		// given
		startDate := "20210230"
		endDate := "20210302"

		// when
		err := ValidateDatePeriod(startDate, endDate)

		// then
		assert.Equal(t, "StartDate is not `yyyyMMdd` Date Format", err.Error())
	})

	t.Run("6자리 앞뒤 날짜 체크 - 종료일이 yyyyMMdd 날짜형식이 아닌경우", func(t *testing.T) {
		// given
		startDate := "20210210"
		endDate := "20210342"

		// when
		err := ValidateDatePeriod(startDate, endDate)

		// then
		assert.Equal(t, "EndDate is not `yyyyMMdd` Date Format", err.Error())
	})

	t.Run("6자리 날짜 체크 - 앞날짜가 늦은경우", func(t *testing.T) {
		// given
		startDate := "20210302"
		endDate := "20210301"

		// when
		err := ValidateDatePeriod(startDate, endDate)

		// then
		assert.Equal(t, "StartDate must be greater than equal EndDate, StartDate : 20210302, EndDate : 20210301", err.Error())
	})
}

func TestValidateInputDateIncludedInPeriod(t *testing.T) {
	t.Run("6자리 입력 날짜의 기간내 포함 체크", func(t *testing.T) {
		// given
		inputDate := "20210302"
		startDate := "20210301"
		endDate := "20210310"

		// when
		err := ValidateInputDateIncludedInPeriod(inputDate, startDate, endDate)

		// then
		assert.Equal(t, nil, err)
	})

	t.Run("6자리 입력 날짜의 기간내 포함 체크 - 입력일이 yyyyMMdd 날짜형식이 아닌경우", func(t *testing.T) {
		// given
		inputDate := "2021033"
		startDate := "20210301"
		endDate := "20210310"

		// when
		err := ValidateInputDateIncludedInPeriod(inputDate, startDate, endDate)

		// then
		assert.Equal(t, "InpuDate is not `yyyyMMdd` Date Format", err.Error())
	})

	t.Run("6자리 입력 날짜의 기간내 포함 체크 - 시작일이 yyyyMMdd 날짜형식이 아닌경우", func(t *testing.T) {
		// given
		inputDate := "20210302"
		startDate := "20210351"
		endDate := "20210310"

		// when
		err := ValidateInputDateIncludedInPeriod(inputDate, startDate, endDate)

		// then
		assert.Equal(t, "StartDate is not `yyyyMMdd` Date Format", err.Error())
	})

	t.Run("6자리 입력 날짜의 기간내 포함 체크 - 종료일이 yyyyMMdd 날짜형식이 아닌경우", func(t *testing.T) {
		// given
		inputDate := "20210302"
		startDate := "20210301"
		endDate := "20210350"

		// when
		err := ValidateInputDateIncludedInPeriod(inputDate, startDate, endDate)

		// then
		assert.Equal(t, "EndDate is not `yyyyMMdd` Date Format", err.Error())
	})

	t.Run("6자리 입력 날짜가 기준첫번쨰 날짜보다 이전", func(t *testing.T) {
		// given
		inputDate := "20210301"
		startDate := "20210302"
		endDate := "20210310"

		// when
		err := ValidateInputDateIncludedInPeriod(inputDate, startDate, endDate)

		// then
		assert.Equal(t, "InputDate must be less than equal startDate, InputDate : 20210301, StartDate : 20210302", err.Error())
	})

	t.Run("6자리 입력 날짜가 기준마지막 날짜보다 이후", func(t *testing.T) {
		// given
		inputDate := "20210312"
		startDate := "20210302"
		endDate := "20210310"

		// when
		err := ValidateInputDateIncludedInPeriod(inputDate, startDate, endDate)

		// then
		assert.Equal(t, "InputDate must be greater than equal EndDate, InputDate : 20210312, EndDate : 20210310", err.Error())
	})
}
