package validator

import (
	"golang-rest-api/common/errors"
	"time"

	"github.com/go-playground/validator/v10"
)

func ValidateDate8(fl validator.FieldLevel) bool {
	value := fl.Field().String()
	return validateDate8(value)
}

func validateDate8(value string) bool {
	if value == "" {
		return true
	}

	_, err := time.Parse("20060102", value)
	if err != nil {
		return false
	}
	return true
}

func ValidateDatePeriod(startDate, endDate string) error {
	startDateFormat, err := time.Parse("20060102", startDate)
	if err != nil {
		return errors.ValidationError("StartDate is not `yyyyMMdd` Date Format")
	}
	endDateFormat, err := time.Parse("20060102", endDate)
	if err != nil {
		return errors.ValidationError("EndDate is not `yyyyMMdd` Date Format")
	}
	if startDateFormat.After(endDateFormat) == true {
		return errors.ValidationError("StartDate must be greater than equal EndDate, StartDate : " + startDate + ", EndDate : " + endDate)
	}
	return nil
}

func ValidateInputDateIncludedInPeriod(inputDate, startDate, endDate string) error {
	inputDateFormat, err := time.Parse("20060102", inputDate)
	if err != nil {
		return errors.ValidationError("InpuDate is not `yyyyMMdd` Date Format")
	}
	startDateFormat, err := time.Parse("20060102", startDate)
	if err != nil {
		return errors.ValidationError("StartDate is not `yyyyMMdd` Date Format")
	}
	endDateFormat, err := time.Parse("20060102", endDate)
	if err != nil {
		return errors.ValidationError("EndDate is not `yyyyMMdd` Date Format")
	}
	if inputDateFormat.Before(startDateFormat) == true {
		return errors.ValidationError("InputDate must be less than equal startDate, InputDate : " + inputDate + ", StartDate : " + startDate)
	}
	if inputDateFormat.After(endDateFormat) == true {
		return errors.ValidationError("InputDate must be greater than equal EndDate, InputDate : " + inputDate + ", EndDate : " + endDate)
	}
	return nil
}
