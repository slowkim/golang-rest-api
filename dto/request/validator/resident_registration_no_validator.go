package validator

import (
	"regexp"

	"github.com/go-playground/validator/v10"
)

func ValidateNativeRegistrationNo(fl validator.FieldLevel) bool {
	value := fl.Field().String()
	return validateNativeRegistrationNo(value)
}

func validateNativeRegistrationNo(value string) bool {
	if value == "" {
		return true
	}
	var validRRN = regexp.MustCompile(`^(?:[0-9]{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[1,2][0-9]|3[0,1]))[1-4][0-9]{6}$`)
	if validRRN.MatchString(value) {
		return true
	}
	return false
}

func ValidateForeignerRegistrationNo(fl validator.FieldLevel) bool {
	value := fl.Field().String()
	return validateForeignerRegistrationNo(value)
}

func validateForeignerRegistrationNo(value string) bool {
	if value == "" {
		return true
	}
	var validRRN = regexp.MustCompile(`^(?:[0-9]{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[1,2][0-9]|3[0,1]))[5-8][0-9]{6}$`)
	if validRRN.MatchString(value) {
		return true
	}
	return false
}
