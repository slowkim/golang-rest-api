package validator

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValidateResidentRegistrationNo(t *testing.T) {
	t.Run("주민등록번호 체크", func(t *testing.T) {
		// given
		nativeRegistrationNo := "9001231245698"

		// when
		result := validateNativeRegistrationNo(nativeRegistrationNo)

		// then
		assert.True(t, result)
	})

	t.Run("주민등록번호 체크 - 공백(required와 함께 쓸 것)", func(t *testing.T) {
		// given
		nativeRegistrationNo := ""

		// when
		result := validateNativeRegistrationNo(nativeRegistrationNo)

		// then
		assert.True(t, result)
	})

	t.Run("주민등록번호 체크 - 문자 포함", func(t *testing.T) {
		// given
		nativeRegistrationNo := "900123124569A"

		// when
		result := validateNativeRegistrationNo(nativeRegistrationNo)

		// then
		assert.False(t, result)
	})

	t.Run("주민등록번호 체크 - 첫6자리가 yyyyMMdd 날짜형식이 아닌경우", func(t *testing.T) {
		// given
		nativeRegistrationNo := "9012332456980"

		// when
		result := validateNativeRegistrationNo(nativeRegistrationNo)

		// then
		assert.False(t, result)
	})

	t.Run("주민등록번호 체크 - yyyyMMdd 날짜형식인 첫6자리 뒤 첫자리(7번쨰숫자)가 1-4 가 아닌경우", func(t *testing.T) {
		// given
		nativeRegistrationNo := "9001235245698"

		// when
		result := validateNativeRegistrationNo(nativeRegistrationNo)

		// then
		assert.False(t, result)
	})
}
