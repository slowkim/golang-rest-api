package validator

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValidateMobile(t *testing.T) {
	t.Run("모바일 번호 체크", func(t *testing.T) {
		// given
		mobileNo := "01000000000"

		// when
		result := validateMobile(mobileNo)

		// then
		assert.True(t, result)
	})

	t.Run("모바일 번호 체크 - 공백(required와 함께 쓸것)", func(t *testing.T) {
		// given
		mobileNo := ""

		// when
		result := validateMobile(mobileNo)

		// then
		assert.True(t, result)
	})

	t.Run("모바일 번호 체크 - 문자 포함", func(t *testing.T) {
		// given
		mobileNo := "0101234567A"

		// when
		result := validateMobile(mobileNo)

		// then
		assert.False(t, result)
	})

	t.Run("모바일 번호 체크 - 010, 011 로 시작하지 않을경우", func(t *testing.T) {
		// given
		mobileNo := "01212345678"

		// when
		result := validateMobile(mobileNo)

		// then
		assert.False(t, result)
	})

	t.Run("모바일 번호 체크 - 010, 011 이후 8자리가 넘는경우", func(t *testing.T) {
		// given
		mobileNo := "010000000009"

		// when
		result := validateMobile(mobileNo)

		// then
		assert.False(t, result)
	})
}

func TestValidateTelNo(t *testing.T) {
	t.Run("전화번호 체크", func(t *testing.T) {
		// given
		telNo := "021234567"

		// when
		result := validateTelNo(telNo)

		// then
		assert.True(t, result)
	})

	t.Run("전화번호 체크 - 공백(required와 함께 쓸것)", func(t *testing.T) {
		// given
		telNo := ""

		// when
		result := validateTelNo(telNo)

		// then
		assert.True(t, result)
	})

	t.Run("전화번호 체크 - 문자 포함", func(t *testing.T) {
		// given
		telNo := "02234567A"

		// when
		result := validateTelNo(telNo)

		// then
		assert.False(t, result)
	})

	t.Run("전화번호 체크 - 11자리가 넘는경우", func(t *testing.T) {
		// given
		telNo := "021234567891"

		// when
		result := validateTelNo(telNo)

		// then
		assert.False(t, result)
	})
}
