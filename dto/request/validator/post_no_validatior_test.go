package validator

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValidatePostNo(t *testing.T) {
	t.Run("우편번호 체크", func(t *testing.T) {
		// given
		postNo := "02365"

		// when
		result := validatePostNo(postNo)

		// then
		assert.True(t, result)
	})

	t.Run("우편번호 체크 - 공백(required와 함께 쓸것)", func(t *testing.T) {
		// given
		postNo := ""

		// when
		result := validatePostNo(postNo)

		// then
		assert.True(t, result)
	})

	t.Run("우편번호 체크 - 문자 포함", func(t *testing.T) {
		// given
		postNo := "4569A"

		// when
		result := validatePostNo(postNo)

		// then
		assert.False(t, result)
	})

	t.Run("우편번호 체크 - 5자리 이상", func(t *testing.T) {
		// given
		postNo := "023656"

		// when
		result := validatePostNo(postNo)

		// then
		assert.False(t, result)
	})

	t.Run("우편번호 체크 - 시작 우편번호가 끝 우편번호보다 크다", func(t *testing.T) {
		// given
		postNoStart := "03000"
		postNoEnd := "02000"

		// when
		err := ValidatePostNoStartGteEnd(postNoStart, postNoEnd)

		// then
		assert.Equal(t, "PostNoStart Greater Than PostNoEnd", err.Error())
	})
}
