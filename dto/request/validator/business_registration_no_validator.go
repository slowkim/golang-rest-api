package validator

import (
	"github.com/go-playground/validator/v10"
)

func ValidateBusinessRegistrationNo(fl validator.FieldLevel) bool {
	value := fl.Field().String()
	return validateBusinessRegistrationNo(value)
}

func validateBusinessRegistrationNo(value string) bool {
	if value == "" {
		return true
	}

	if len(value) == 10 {
		key := "137137135"
		sum := 0
		for i := 0; i <= 8; i++ {
			sum += int(value[i]-48) * int(key[i]-48)
		}
		num := 10 - (sum+(int(value[8]-48)*int(key[8]-48))/10)%10
		if int(value[9]-48) == num {
			return true
		}
		return false
	}

	return false
}
