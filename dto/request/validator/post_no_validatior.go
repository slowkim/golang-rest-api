package validator

import (
	"golang-rest-api/common/errors"
	"regexp"

	"github.com/go-playground/validator/v10"
)

func ValidatePostNo(fl validator.FieldLevel) bool {
	value := fl.Field().String()
	return validatePostNo(value)
}

func validatePostNo(value string) bool {
	if value == "" {
		return true
	}

	var validPostNo = regexp.MustCompile(`^\d{5}$`)
	if validPostNo.MatchString(value) {
		return true
	}
	return false
}

func ValidatePostNoStartGteEnd(postNoStart, postNoEnd string) error {
	if !validatePostNo(postNoStart) {
		return errors.ValidationError("Invalid PostNo")
	}
	if !validatePostNo(postNoEnd) {
		return errors.ValidationError("Invalid PostNo")
	}
	if postNoStart > postNoEnd {
		return errors.ValidationError("PostNoStart Greater Than PostNoEnd")
	}
	return nil
}
