package dto

import (
	adminEnum "golang-rest-api/administrator/enum"

	"github.com/labstack/echo/v4"
)

type AdministratorCreate struct {
	Email    string                      `json:"email" validate:"required,email"`
	Mobile   string                      `json:"mobile" validate:"lte=11,required,mobileNo"`
	Password string                      `json:"password" validate:"gte=6,lte=100"`
	RoleType adminEnum.AdminRoleTypeEnum `json:"roleType" validate:"lte=50,required"`
}

func (a AdministratorCreate) Validate(ctx echo.Context) (err error) {
	if err = ctx.Validate(a); err != nil {
		return err
	}

	return
}

type AdministratorUpdate struct {
	Id       int64                       `json:"id" validate:"required"`
	Email    string                      `json:"email" validate:"required,email"`
	Mobile   string                      `json:"mobile" validate:"required,mobileNo"`
	Password string                      `json:"password" validate:"gte=6,lte=100"`
	RoleType adminEnum.AdminRoleTypeEnum `json:"roleType" validate:"lte=50,required"`
}

func (a AdministratorUpdate) Validate(ctx echo.Context) (err error) {
	if err = ctx.Validate(a); err != nil {
		return err
	}
	return
}

type SearchAdminQueryParams struct {
	Email  string `query:"email" `
	Mobile string `query:"mobile" `
}
