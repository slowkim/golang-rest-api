package dto

type SearchMemberQueryParams struct {
	Nickname string `json:"nickname" `
	Mobile   string `json:"mobile" `
}
