package entity

import (
	"time"
)

type ErrorLog struct {
	Id            int64                  `xorm:"id pk autoincr" `
	Hostname      string                 `xorm:"host_name"`
	Service       string                 `xorm:"service"`
	Code          string                 `xorm:"code"`
	Error         string                 `xorm:"error"`
	StackTrace    string                 `xorm:"stack_trace"`
	RemoteIP      string                 `xorm:"remote_ip"`
	Host          string                 `xorm:"host"`
	Uri           string                 `xorm:"uri"`
	Method        string                 `xorm:"method"`
	Path          string                 `xorm:"path"`
	Referer       string                 `xorm:"referer"`
	UserAgent     string                 `xorm:"user_agent"`
	Status        int                    `xorm:"status"`
	RequestLength int64                  `xorm:"request_length"`
	BytesSent     int64                  `xorm:"bytes_sent"`
	Body          interface{}            `xorm:"body"`
	Params        map[string]interface{} `xorm:"params"`
	Controller    string                 `xorm:"controller"`
	Action        string                 `xorm:"action"`
	UserId        int64                  `xorm:"userId"`
	AuthToken     string                 `xorm:"auth_token"`
	Timestamp     time.Time              `xorm:"timestamp"`
	CreatedAt     time.Time              `xorm:"created" `
}

func (ErrorLog) TableName() string {
	return "error_logs"
}

func (e *ErrorLog) SetUserClaimErrorLog(userClaimId int64) {
	if userClaimId != 0 {
		e.UserId = userClaimId
	}
}
