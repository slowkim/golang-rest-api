package mapper

import (
	"context"
	requestDto "golang-rest-api/dto/request"
	"golang-rest-api/logger/entity"
)

func NewErrorLog(ctx context.Context, creation requestDto.ErrorLog) (entity.ErrorLog, error) {
	return entity.ErrorLog{
		Hostname:      creation.Hostname,
		Service:       creation.Service,
		Code:          creation.Code,
		Error:         creation.Error,
		StackTrace:    creation.StackTrace,
		Timestamp:     creation.Timestamp,
		RemoteIP:      creation.RemoteIP,
		Host:          creation.Host,
		Uri:           creation.Uri,
		Method:        creation.Method,
		Path:          creation.Path,
		Referer:       creation.Referer,
		UserAgent:     creation.UserAgent,
		Status:        creation.Status,
		RequestLength: creation.RequestLength,
		BytesSent:     creation.BytesSent,
		Body:          creation.Body,
		Params:        creation.Params,
		Controller:    creation.Controller,
		Action:        creation.Action,
		UserId:        creation.UserId,
		AuthToken:     creation.AuthToken,
	}, nil
}
