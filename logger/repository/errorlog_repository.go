package repository

import (
	"context"
	"golang-rest-api/common"
	"golang-rest-api/logger/entity"
	"sync"
)

var (
	errorLogRepositoryOnce     sync.Once
	errorLogRepositoryInstance *errorLogRepository
)

func ErrorLogRepository() *errorLogRepository {
	errorLogRepositoryOnce.Do(func() {
		errorLogRepositoryInstance = &errorLogRepository{}
	})
	return errorLogRepositoryInstance
}

type errorLogRepository struct {
}

func (errorLogRepository) Create(ctx context.Context, errorLog *entity.ErrorLog) (err error) {
	if _, err = common.GetDB(ctx).Insert(errorLog); err != nil {
		return
	}

	return
}
