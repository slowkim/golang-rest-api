package service

import (
	"context"
	requestDto "golang-rest-api/dto/request"
	"golang-rest-api/logger/mapper"
	"golang-rest-api/logger/repository"
	"sync"
)

var (
	errorLogRepositoryOnce     sync.Once
	errorLogRepositoryInstance *errorLogRepository
)

func ErrorLogService() *errorLogRepository {
	errorLogRepositoryOnce.Do(func() {
		errorLogRepositoryInstance = &errorLogRepository{}
	})

	return errorLogRepositoryInstance
}

type errorLogRepository struct {
}

func (errorLogRepository) Create(ctx context.Context, creation requestDto.ErrorLog) (err error) {
	errorLog, err := mapper.NewErrorLog(ctx, creation)
	if err != nil {
		return
	}

	errorLog.SetUserClaimErrorLog(int64(0))

	if repository.ErrorLogRepository().Create(ctx, &errorLog); err != nil {
		return
	}

	return
}
