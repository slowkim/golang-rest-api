package adapter

import (
	"golang-rest-api/config"
	"os"
	"runtime"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestKakaoMessagingAdapter_GetApiToken(t *testing.T) {
	// given
	runtime.GOMAXPROCS(1)
	os.Setenv("CONFIGOR_ENV", "test")
	os.Setenv("KAKAO_MESSAGING_ACCOUNT_PASSWORD", "kakao_password")
	config.ConfigureEnvironment("../", "KAKAO_MESSAGING_ACCOUNT_PASSWORD")

	// when
	token, err := kakaoMessagingAdapter{}.getApiToken()

	// then
	if err != nil {
		t.Error(err)
	}

	assert.True(t, len(token["accessToken"].(string)) > 0)
	assert.True(t, len(token["schema"].(string)) > 0)
}

func TestKakaoMessagingAdapter_SendDonationReservedByCUPOSTBOXMessage(t *testing.T) {
	t.Skip()
	// given
	runtime.GOMAXPROCS(1)
	os.Setenv("CONFIGOR_ENV", "test")
	os.Setenv("KAKAO_MESSAGING_ACCOUNT_PASSWORD", "kakao_password")
	config.ConfigureEnvironment("../", "KAKAO_MESSAGING_ACCOUNT_PASSWORD")

	cellPhoneNo := "01000000000"
	reservedNo := "3132101240013"
	nickName := "테스터"

	// when
	err := kakaoMessagingAdapter{}.SendDMessage(cellPhoneNo, reservedNo, nickName)

	// then
	assert.Nil(t, err)
}
