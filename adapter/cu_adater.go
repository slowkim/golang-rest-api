package adapter

import (
	"context"
	"golang-rest-api/common"
	"golang-rest-api/common/errors"
	"golang-rest-api/config"
	requestDto "golang-rest-api/dto/request"
	"sync"
	"time"

	"github.com/bettercode-oss/rest"
)

var (
	cuAdapterOnce     sync.Once
	cuAdapterInstance *cuAdapter
)

func CuAdapter() *cuAdapter {
	cuAdapterOnce.Do(func() {
		cuAdapterInstance = &cuAdapter{}
	})

	return cuAdapterInstance
}

const CU_SUCCESS_CODE = "00"
const CU_CANCEL_NOT_AVALIABLE = "01"
const CU_UNREGISTREED_CUST = "90"
const CU_UNREGISTREED_SERVICE = "91"
const CU_ERROR_CODE = "99"

type cuAdapter struct {
}

func (cuAdapter) MakeReservation(ctx context.Context) (err error) {
	itemCategory := "의류/잡화"
	requestObject := requestDto.CUPostReservation{
		ComGb:        "1",
		CustOrdNo:    "예약번호",
		AcptDt:       time.Now().Format(common.DateLayout8),
		SendTel:      "보내는사람 핸드폰번호",
		SendName:     common.RuneStringSplit("보내는사람", 15),
		SendZip:      "보내는사람우편번호",
		SendAddr1:    common.RuneStringSplit("보내는사람주소", 40),
		SendAddr2:    common.RuneStringSplit("보내는사람주소상세", 40),
		GoodsNo:      "",
		Goods:        itemCategory,
		GoodsCnt:     5,
		Extra:        " ",
		GoodsPrice:   500000,
		ReceiveName:  common.RuneStringSplit("받는장소", 15),
		ReceiveTel:   "",
		ReceiveZip:   "",
		ReceiveAddr1: common.RuneStringSplit("받는주소", 40),
		ReceiveAddr2: common.RuneStringSplit("받는주소상세", 40),
		TLSGB:        1,
		GoodsInfo:    itemCategory,
		OrderNum:     "",
		LimitWgt:     5,
		CancelFlag:   "N",
		DcAmt:        0,
		SmsYn:        "N",
	}

	cuPostReserved := struct {
		ResCd  string `json:"res_cd"`
		ResMsg string `json:"res_msg"`
	}{}

	client := rest.Client{
		ShowHttpLog: config.Config.Cu.ShowHttpLog,
		RetryMax:    5,
		RetryDelay:  1 * time.Second,
	}
	url := config.Config.Cu.PostBoxURL + "/shopping_rest/tranProc.cupost"
	err = client.
		Request().
		SetHeader("Accept", "application/json").
		SetHeader("cust_no", config.Config.Cu.CustNo).
		SetBody(requestObject).
		SetResult(&cuPostReserved).
		Post(url)
	if err != nil {
		return errors.Throw(err)
	}

	returnCode := cuPostReserved.ResCd

	if returnCode != CU_SUCCESS_CODE {
		return errors.ErrNotValid
	}

	return
}
