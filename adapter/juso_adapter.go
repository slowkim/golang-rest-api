package adapter

import (
	"encoding/json"
	"fmt"
	"golang-rest-api/config"
	requestDto "golang-rest-api/dto/request"
	responseDto "golang-rest-api/dto/response"
	"io/ioutil"
	"net/http"
	"net/url"
	"sync"
)

var (
	jusoAdapterOnce     sync.Once
	jusoAdapterInstance *jusoAdapter
)

func JusoAdapter() *jusoAdapter {
	jusoAdapterOnce.Do(func() {
		jusoAdapterInstance = &jusoAdapter{}
	})

	return jusoAdapterInstance
}

type jusoAdapter struct {
}

func (adpater jusoAdapter) SearchAddress(query string, pageable requestDto.Pageable) ([]responseDto.Address, error) {
	RestApiKey := config.Config.Juso.JusoApiKey
	url := fmt.Sprintf(`http://www.juso.go.kr/addrlink/addrLinkApi.do?confmKey=%s&currentPage=%d&countPerPage=%d&resultType=json&keyword=%v`, RestApiKey, pageable.Page, pageable.PageSize, url.QueryEscape(query))
	req, _ := http.NewRequest("GET", url, nil)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var addressMap = map[string]interface{}{}
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if err := json.Unmarshal(b, &addressMap); err != nil {
		return nil, err
	}

	return adpater.translateToAddress(addressMap), nil
}

func (jusoAdapter) translateToAddress(addressMap map[string]interface{}) []responseDto.Address {
	var searchAddressList = make([]responseDto.Address, 0)
	addressMapResult := addressMap["results"].(map[string]interface{})
	common := addressMapResult["common"].(map[string]interface{})
	errorCode := common["errorCode"].(string)

	if errorCode != "0" {
		return searchAddressList
	}

	documents := addressMapResult["juso"].([]interface{})
	for _, document := range documents {
		documentMap := document.(map[string]interface{})
		addressName := documentMap["jibunAddr"].(string)
		loadAddressName := documentMap["roadAddr"].(string)
		loadZoneNo := documentMap["zipNo"].(string)

		searchAddressList = append(searchAddressList, responseDto.Address{
			AddressName:     addressName,
			LoadAddressName: loadAddressName,
			LoadZoneNo:      loadZoneNo,
		})
	}

	return searchAddressList
}
