package adapter

import (
	"context"
	"golang-rest-api/config"
	"time"

	"github.com/Shopify/sarama"
	"gitlab.com/slowkim/golang-kafka/kafka"
)

var (
	EventMessagePublisher *KafkaMessagePublisher
	EventMessageConsumer  *KafkaMessageComsumer
)

type KafkaMessagePublisher struct {
	producer *kafka.Producer
}

type KafkaMessageComsumer struct {
	consumerGroup *kafka.ConsumerGroupHandler
}

func NewKafkaMessagePublisher() (*KafkaMessagePublisher, error) {
	producer, err := kafka.NewProducer(config.Config.EventMessageBroker.Kafka.Brokers, config.Config.EventMessageBroker.Kafka.Topic, func(c *sarama.Config) {
		c.Producer.RequiredAcks = sarama.WaitForLocal       // Only wait for the leader to ack
		c.Producer.Compression = sarama.CompressionGZIP     // Compress messages
		c.Producer.Flush.Frequency = 500 * time.Millisecond // Flush batches every 500ms
	})

	if err != nil {
		return nil, err
	}

	kafkaMessagePublisher := KafkaMessagePublisher{
		producer: producer,
	}

	return &kafkaMessagePublisher, nil
}

func (publisher KafkaMessagePublisher) Close() {
	publisher.producer.Close()
}

func (publisher KafkaMessagePublisher) Publish(message interface{}, key string) error {
	if err := publisher.producer.SendWithKey(message, key); err != nil {
		return err
	}

	return nil
}

func NewConsumerGroup() (*KafkaMessageComsumer, error) {
	ConsumerGroup, err := kafka.NewConsumerGroup(config.Config.Service.Name, config.Config.EventMessageBroker.Kafka.Brokers, config.Config.EventMessageBroker.Kafka.Topic)
	if err != nil {
		return nil, err
	}

	kafkaMessageComsumer := KafkaMessageComsumer{
		consumerGroup: ConsumerGroup,
	}

	return &kafkaMessageComsumer, nil
}

func (consumer KafkaMessageComsumer) Messages() (consumerMessages <-chan *sarama.ConsumerMessage, err error) {
	consumerMessages, err = consumer.consumerGroup.Messages()
	if err != nil {
		return nil, err
	}

	return
}
func TranslateKafkaSendMessage(ctx context.Context, actionType string, payLoad interface{}) map[string]interface{} {
	return map[string]interface{}{
		"actionType": actionType,
		"payload":    payLoad,
		"createdAt":  time.Now().UTC(),
	}
}
