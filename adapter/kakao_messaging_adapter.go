package adapter

import (
	"fmt"
	"golang-rest-api/config"
	"strings"
	"sync"
	"time"

	"github.com/bettercode-oss/rest"
)

const KoreaCountryCode = "82"

var (
	kakaoMessagingAdapterOnce     sync.Once
	kakaoMessagingAdapterInstance *kakaoMessagingAdapter
)

func KakaoMessagingAdapter() *kakaoMessagingAdapter {
	kakaoMessagingAdapterOnce.Do(func() {
		kakaoMessagingAdapterInstance = &kakaoMessagingAdapter{}
	})

	return kakaoMessagingAdapterInstance
}

type kakaoMessagingAdapter struct {
}

func (adapter kakaoMessagingAdapter) getApiToken() (token map[string]interface{}, err error) {
	client := rest.Client{
		ShowHttpLog: config.Config.Cu.ShowHttpLog,
		RetryMax:    5,
		RetryDelay:  1 * time.Second,
	}

	err = client.
		Request().
		SetHeader("Accept", "application/json").
		SetHeader("X-IB-Client-Id", config.Config.KakaoMessaging.Account.Id).
		SetHeader("X-IB-Client-Passwd", config.Config.KakaoMessaging.Account.Password).
		SetResult(&token).
		Post(config.Config.KakaoMessaging.AuthUrl)
	if err != nil {
		return
	}

	return token, nil
}

func (adapter kakaoMessagingAdapter) sendMessage(requestBody map[string]interface{}) (err error) {
	token, err := adapter.getApiToken()
	authorization := fmt.Sprintf("%s %s", token["schema"].(string), token["accessToken"].(string))
	client := rest.Client{
		ShowHttpLog: config.Config.Cu.ShowHttpLog,
		RetryMax:    5,
		RetryDelay:  1 * time.Second,
	}

	err = client.
		Request().
		SetHeader("Accept", "application/json").
		SetHeader("X-IB-Client-Id", config.Config.KakaoMessaging.Account.Id).
		SetHeader("X-IB-Client-Passwd", config.Config.KakaoMessaging.Account.Password).
		SetHeader("Authorization", authorization).
		SetBody(requestBody).
		Post(config.Config.KakaoMessaging.SendingUrl)
	if err != nil {
		return
	}

	return nil
}

func (kakaoMessagingAdapter) convertKakaoSendingNo(cellPhone string) string {
	if strings.HasPrefix(cellPhone, "0") {
		return fmt.Sprintf("%s%s", KoreaCountryCode, cellPhone[1:])
	} else {
		return fmt.Sprintf("%s%s", KoreaCountryCode, cellPhone)
	}
}

func (adapter kakaoMessagingAdapter) SendDMessage(cellPhoneNumber string, reservedNo string, nickName string) error {
	domain := config.Config.KakaoMessaging.MessageSendingDomain
	requestBody := map[string]interface{}{
		"msg_type": "AL",
		"msg_data": map[string]interface{}{
			"to":      adapter.convertKakaoSendingNo(cellPhoneNumber),
			"content": fmt.Sprintf("예약번호:%s \n\n%s님의 예약이 완료되었습니다.", reservedNo, nickName),
			"title":   fmt.Sprintf("%s", reservedNo),
		},
		"msg_attr": map[string]interface{}{
			"sender_key":      config.Config.KakaoMessaging.KakaoChannelSenderKey,
			"template_code":   "SV1.0001",
			"response_method": "realtime",
			"content_type":    "V",
			"attachment": map[string]interface{}{
				"button": []interface{}{
					map[string]interface{}{
						"name":       "신청내역 확인하기",
						"type":       "WL",
						"url_pc":     fmt.Sprintf("https://%s/mypage", domain),
						"url_mobile": fmt.Sprintf("https://%s/mypage", domain),
					},
				},
			},
		},
	}

	if err := adapter.sendMessage(requestBody); err != nil {
		return err
	}

	return nil
}
