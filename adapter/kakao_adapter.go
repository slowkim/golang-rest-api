package adapter

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"golang-rest-api/config"
	responseDto "golang-rest-api/dto/response"
	"io/ioutil"
	"net/http"
	"net/url"
	"sync"
)

var (
	kakaoAdapterOnce     sync.Once
	kakaoAdapterInstance *kakaoAdapter
)

func KakaoAdapter() *kakaoAdapter {
	kakaoAdapterOnce.Do(func() {
		kakaoAdapterInstance = &kakaoAdapter{}
	})

	return kakaoAdapterInstance
}

type kakaoAdapter struct {
}

func (adpater kakaoAdapter) SearchPlaces(query string) ([]responseDto.AddressWithPosition, error) {
	url := fmt.Sprintf(`https://dapi.kakao.com/v2/local/search/keyword.json?page=1&size=15&sort=accuracy&query=%v`, url.QueryEscape(query))
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("Authorization", fmt.Sprintf(`KakaoAK %v`, config.Config.Kakao.RestApiKey)) // 헤더값 설정

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var addressMap = map[string]interface{}{}
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if err := json.Unmarshal(b, &addressMap); err != nil {
		return nil, err
	}

	return adpater.translateToAddressWithPosition(addressMap), nil
}

func (kakaoAdapter) translateToAddressWithPosition(addressMap map[string]interface{}) []responseDto.AddressWithPosition {
	var addressWithPositionList = make([]responseDto.AddressWithPosition, 0)

	documents := addressMap["documents"].([]interface{})

	for _, document := range documents {
		documentMap := document.(map[string]interface{})
		placeName := documentMap["place_name"].(string)
		addressName := ""
		if documentMap["road_address_name"].(string) == "" {
			addressName = documentMap["address_name"].(string)
		} else {
			addressName = documentMap["road_address_name"].(string)
		}
		phone := documentMap["phone"].(string)
		longitude := documentMap["x"].(string)
		latitude := documentMap["y"].(string)

		addressWithPositionList = append(addressWithPositionList, responseDto.AddressWithPosition{
			PlaceName:   placeName,
			AddressName: addressName,
			Phone:       phone,
			Longitude:   longitude,
			Latitude:    latitude,
		})
	}

	return addressWithPositionList
}

func (kakaoAdapter) CoordToAddress(latitude string, longitude string) (string, error) {
	// curl -X GET "https://dapi.kakao.com/v2/local/geo/coord2address.json?input_coord=WGS84&y=37.6471552&x=127.074304" \
	//	-H "Authorization: KakaoAK {REST_API_KEY}"

	url := fmt.Sprintf(`https://dapi.kakao.com/v2/local/geo/coord2address.json?input_coord=WGS84&y=%v&x=%v`, latitude, longitude)
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("Authorization", fmt.Sprintf(`KakaoAK %v`, config.Config.Kakao.RestApiKey)) // 헤더값 설정

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	var addressMap = map[string]interface{}{}
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	if err := json.Unmarshal(b, &addressMap); err != nil {
		return "", err
	}

	documents := addressMap["documents"].([]interface{})
	if documents != nil && len(documents) > 0 {
		documentMap := documents[0].(map[string]interface{})
		addressMap := documentMap["address"].(map[string]interface{})
		address := addressMap["address_name"].(string)
		return address, nil
	}
	return "", nil
}

func (kakaoAdapter) GetKakaoUserInfo(authorizeCode string) (map[string]interface{}, error) {
	// 1.인증 코드로 사용 토큰을 받기
	// https://developers.kakao.com/docs/latest/ko/kakaologin/rest-api#request-token
	data := url.Values{}
	data.Add("grant_type", "authorization_code")
	data.Add("client_id", config.Config.Kakao.RestApiKey)
	data.Add("redirect_uri", config.Config.Kakao.RedirectURL)
	data.Add("code", authorizeCode)

	response, err := http.Post("https://kauth.kakao.com/oauth/token", "application/x-www-form-urlencoded;charset=utf-8", bytes.NewBufferString(data.Encode()))
	defer response.Body.Close()
	if err != nil {
		return nil, err
	}

	b, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	var userToken = map[string]interface{}{}
	if err := json.Unmarshal(b, &userToken); err != nil {
		return nil, err
	}

	// 2. 토큰으로 사용자 정보 요청
	// https://developers.kakao.com/docs/latest/ko/user-mgmt/rest-api#req-user-info
	req, err := http.NewRequest(http.MethodPost, "https://kapi.kakao.com/v2/user/me", nil)
	if err != nil {
		return nil, err
	}
	if userToken["access_token"] == nil {
		return nil, errors.New("kakao api get error : access_token is null")
	}
	req.Header.Set("Authorization", "Bearer "+userToken["access_token"].(string))

	client := &http.Client{}
	response, err = client.Do(req)
	if err != nil {
		return nil, err
	}

	defer response.Body.Close()
	b, err = ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	fmt.Println(string(b))

	var userInfo = map[string]interface{}{}
	if err := json.Unmarshal(b, &userInfo); err != nil {
		return nil, err
	}

	return userInfo, nil
}

func (adpater kakaoAdapter) SearchAddress(query string) ([]responseDto.Address, error) {
	url := fmt.Sprintf(`https://dapi.kakao.com/v2/local/search/address.json?query=%v`, url.QueryEscape(query))
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("Authorization", fmt.Sprintf(`KakaoAK %v`, config.Config.Kakao.RestApiKey)) // 헤더값 설정

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var addressMap = map[string]interface{}{}
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if err := json.Unmarshal(b, &addressMap); err != nil {
		return nil, err
	}

	return adpater.translateToAddress(addressMap), nil
}

func (kakaoAdapter) translateToAddress(addressMap map[string]interface{}) []responseDto.Address {
	var searchAddressList = make([]responseDto.Address, 0)
	documents := addressMap["documents"].([]interface{})
	for _, document := range documents {
		documentMap := document.(map[string]interface{})
		addressName := documentMap["address_name"].(string)

		loadAddressName := ""
		loadZoneNo := ""
		if documentMap["road_address"] != nil {
			loadAddressMap := documentMap["road_address"].(map[string]interface{})
			loadAddressName = loadAddressMap["address_name"].(string)
			loadZoneNo = loadAddressMap["zone_no"].(string)
			if loadZoneNo == "" {
				continue
			}
			searchAddressList = append(searchAddressList, responseDto.Address{
				AddressName:     addressName,
				LoadAddressName: loadAddressName,
				LoadZoneNo:      loadZoneNo,
			})
		}
	}

	return searchAddressList
}
