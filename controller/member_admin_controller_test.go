package controller

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	auth "golang-rest-api/config/authentication"
	responseDto "golang-rest-api/dto/response"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

func TestAdminMemberController_Get(t *testing.T) {
	DatabaseFixture{}.setUpDefault(xormDb.Engine)

	t.Run("GetMember", func(t *testing.T) {
		// given
		memberId := "2"
		req := httptest.NewRequest(echo.GET, fmt.Sprintf("/api/admin/members/:id"), nil)
		req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)
		userClaim := auth.UserClaim{
			Id:    2,
			Roles: "admin",
		}

		// when
		rec := NewRequest(req).
			WithUser(&userClaim).
			WithParam("id", memberId).
			Handle(MemberAdminController{}.GetMember)

		// then
		assert.Equal(t, http.StatusOK, rec.Code)
		result := responseDto.MemberSummary{}
		json.Unmarshal(rec.Body.Bytes(), &result)

		assert.Equal(t, "2thMember", result.Nickname)
	})

	t.Run("GetMember - nothing member", func(t *testing.T) {
		// given
		memberId := "1000"
		req := httptest.NewRequest(echo.GET, fmt.Sprintf("/api/admin/members/:id"), nil)
		req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)
		userClaim := auth.UserClaim{
			Id:    2,
			Roles: "admin",
		}

		// when
		rec := NewRequest(req).
			WithUser(&userClaim).
			WithParam("id", memberId).
			Handle(MemberAdminController{}.GetMember)

		// then
		assert.Equal(t, http.StatusNotFound, rec.Code)

		result := responseDto.ApiError{}
		json.Unmarshal(rec.Body.Bytes(), &result)

		assert.Equal(t, 10003, result.Code)
		assert.Equal(t, "결과를 찾을 수 없습니다.", result.Message)
	})

	t.Run("GetMembers", func(t *testing.T) {
		// given
		req := httptest.NewRequest(echo.GET, fmt.Sprintf("/api/admin/members"), nil)
		req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)
		userClaim := auth.UserClaim{
			Id:    2,
			Roles: "admin",
		}

		// when
		rec := NewRequest(req).
			WithUser(&userClaim).
			Handle(MemberAdminController{}.GetMembers)

		// then
		assert.Equal(t, http.StatusOK, rec.Code)
		result := responseDto.PageResult{}
		json.Unmarshal(rec.Body.Bytes(), &result)

		assert.Equal(t, int64(5), result.TotalCount)
	})
}
