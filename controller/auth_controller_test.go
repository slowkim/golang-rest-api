package controller

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	responseDto "golang-rest-api/dto/response"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

func TestAuthController_login(t *testing.T) {
	DatabaseFixture{}.setUpDefault(xormDb.Engine)

	t.Run("Login", func(t *testing.T) {
		// given
		adminUser := map[string]interface{}{
			"email":    "slowkim@gmail.com",
			"password": "a042d70e-27dc",
		}

		requestBody, err := json.Marshal(adminUser)
		if err != nil {
			t.Error(err)
			t.Fail()
		}

		req := httptest.NewRequest(echo.POST, fmt.Sprintf("/api/auth/login"), bytes.NewReader(requestBody))
		req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)

		// when
		rec := NewRequest(req).
			Handle(AuthController{}.AuthAdminWithEmailAndPassword)

		// then
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.NotEmpty(t, rec.Body.String)
	})

	t.Run("Login - incorrect email", func(t *testing.T) {
		// given
		adminUser := map[string]interface{}{
			"email":    "1234@gmail.com",
			"password": "a042d70e-27dc",
		}

		requestBody, err := json.Marshal(adminUser)
		if err != nil {
			t.Error(err)
			t.Fail()
		}

		req := httptest.NewRequest(echo.POST, fmt.Sprintf("/api/auth/login"), bytes.NewReader(requestBody))
		req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)

		// when
		rec := NewRequest(req).
			Handle(AuthController{}.AuthAdminWithEmailAndPassword)

		// then
		assert.Equal(t, http.StatusBadRequest, rec.Code)

		result := responseDto.ApiError{}
		json.Unmarshal(rec.Body.Bytes(), &result)
		assert.Equal(t, 10008, result.Code)
		assert.Equal(t, "잘못된 요청입니다.", result.Message)
	})

	t.Run("Login - incorrect password", func(t *testing.T) {
		// given
		adminUser := map[string]interface{}{
			"email":    "admin@gmail.com",
			"password": "11112233",
		}

		requestBody, err := json.Marshal(adminUser)
		if err != nil {
			t.Error(err)
			t.Fail()
		}

		req := httptest.NewRequest(echo.POST, fmt.Sprintf("/api/auth/login"), bytes.NewReader(requestBody))
		req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)

		// when
		rec := NewRequest(req).
			Handle(AuthController{}.AuthAdminWithEmailAndPassword)

		// then
		assert.Equal(t, http.StatusBadRequest, rec.Code)

		result := responseDto.ApiError{}
		json.Unmarshal(rec.Body.Bytes(), &result)
		assert.Equal(t, 10008, result.Code)
		assert.Equal(t, "잘못된 요청입니다.", result.Message)
	})
}
