package controller

import (
	"fmt"
	"golang-rest-api/auth/service"
	"golang-rest-api/common/errors"
	"golang-rest-api/config"
	requestDto "golang-rest-api/dto/request"
	"net/http"

	"github.com/labstack/echo/v4"
)

type AuthController struct {
}

func (controller AuthController) Init(g *echo.Group) {
	g.GET("/kakao", controller.RedirectKakaoLoginPage)
	g.GET("", controller.AuthWithKakao)
	g.POST("/login", controller.AuthAdminWithEmailAndPassword)
}

func (AuthController) RedirectKakaoLoginPage(ctx echo.Context) (err error) {
	kakaoLoginUrl := fmt.Sprintf("https://kauth.kakao.com/oauth/authorize?client_id=%s&redirect_uri=%s&response_type=code",
		config.Config.Kakao.RestApiKey, config.Config.Kakao.RedirectURL)

	return ctx.Redirect(http.StatusPermanentRedirect, kakaoLoginUrl)
}

func (AuthController) AuthWithKakao(ctx echo.Context) (err error) {
	authorizeCode := ctx.QueryParam("code")
	kakaoErr := ctx.QueryParam("error")

	loginUri := config.Config.WebApp.LoginUrl

	if len(kakaoErr) > 0 {
		return ctx.Redirect(http.StatusMovedPermanently, fmt.Sprintf("%s?error=%s", loginUri, kakaoErr))
	}

	var redirectUri string

	isSignUp, token, err := service.AuthService().NewMemberJwtToken(ctx.Request().Context(), authorizeCode)
	if err != nil {
		redirectUri = fmt.Sprintf("%s?error=%s", loginUri, err.Error())
	} else if isSignUp {
		redirectUri = fmt.Sprintf("%s?token=%s&isSignUp=true", loginUri, token)
	} else {
		redirectUri = fmt.Sprintf("%s?token=%s&isSignUp=false", loginUri, token)
	}

	return ctx.Redirect(http.StatusMovedPermanently, redirectUri)
}

func (AuthController) AuthAdminWithEmailAndPassword(ctx echo.Context) (err error) {
	var adminSignIn requestDto.AdminSignIn
	if err := ctx.Bind(&adminSignIn); err != nil {
		return errors.ApiParamValidError(err)
	}

	if err := adminSignIn.Validate(ctx); err != nil {
		return err
	}

	adminJwtToken, err := service.AuthService().NewAdminJwtToken(ctx.Request().Context(), adminSignIn)
	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, adminJwtToken)
}
