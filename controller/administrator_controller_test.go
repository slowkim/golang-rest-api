package controller

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"golang-rest-api/administrator/enum"
	"golang-rest-api/common/errors"
	auth "golang-rest-api/config/authentication"
	responseDto "golang-rest-api/dto/response"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

func TestAdministratorController_Get(t *testing.T) {
	DatabaseFixture{}.setUpDefault(xormDb.Engine)

	t.Run("GetAdministrator", func(t *testing.T) {
		// given
		adminId := "1"
		req := httptest.NewRequest(echo.GET, fmt.Sprintf("/api/admin/administrators/:id"), nil)
		req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)
		userClaim := auth.UserClaim{
			Id:    1,
			Roles: "admin",
		}

		// when
		rec := NewRequest(req).
			WithUser(&userClaim).
			WithParam("id", adminId).
			Handle(AdministratorController{}.GetAdministrator)

		// then
		assert.Equal(t, http.StatusOK, rec.Code)
		result := responseDto.AdministratorSummary{}
		json.Unmarshal(rec.Body.Bytes(), &result)

		assert.Equal(t, "slowkim@gmail.com", result.Email)
	})

	t.Run("GetAdministrator - nothing", func(t *testing.T) {
		// given
		adminId := "100"
		req := httptest.NewRequest(echo.GET, fmt.Sprintf("/api/admin/administrators/:id"), nil)
		req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)
		userClaim := auth.UserClaim{
			Id:    1,
			Roles: "admin",
		}

		// when
		rec := NewRequest(req).
			WithUser(&userClaim).
			WithParam("id", adminId).
			Handle(AdministratorController{}.GetAdministrator)

		// then
		assert.Equal(t, http.StatusNotFound, rec.Code)

		result := responseDto.ApiError{}
		json.Unmarshal(rec.Body.Bytes(), &result)

		assert.Equal(t, 10003, result.Code)
		assert.Equal(t, "결과를 찾을 수 없습니다.", result.Message)
	})

	t.Run("GetAdministrators", func(t *testing.T) {
		// given
		req := httptest.NewRequest(echo.GET, fmt.Sprintf("/api/admin/administrators"), nil)
		req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)

		userClaim := auth.UserClaim{
			Id:    1,
			Roles: "admin",
		}

		// when
		rec := NewRequest(req).
			WithUser(&userClaim).
			Handle(AdministratorController{}.GetAdministrators)

		// then
		assert.Equal(t, http.StatusOK, rec.Code)
		result := responseDto.PageResult{}
		json.Unmarshal(rec.Body.Bytes(), &result)

		assert.Equal(t, int64(3), result.TotalCount)
	})

	t.Run("GetAdministrators - email - slowkim@gmail.com", func(t *testing.T) {
		// given
		req := httptest.NewRequest(echo.GET, fmt.Sprintf("/api/admin/administrators?email=slowkim@gmail.com"), nil)
		req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)

		userClaim := auth.UserClaim{
			Id:    1,
			Roles: "admin",
		}

		// when
		rec := NewRequest(req).
			WithUser(&userClaim).
			Handle(AdministratorController{}.GetAdministrators)

		// then
		assert.Equal(t, http.StatusOK, rec.Code)
		result := responseDto.PageResult{}
		json.Unmarshal(rec.Body.Bytes(), &result)

		assert.Equal(t, int64(1), result.TotalCount)
	})

}

func TestAdministratorController_Create(t *testing.T) {
	DatabaseFixture{}.setUpDefault(xormDb.Engine)

	t.Run("Create Administrator - Normal case", func(t *testing.T) {
		// given
		requestBody := `{
			"email":"admin5@gmail.com",
			"password":"123456",
			"mobile":"01000000000",
			"roleType": "` + enum.BSTORE.String() + `"
		}`
		req := httptest.NewRequest(echo.POST, fmt.Sprintf("/api/admin/administrators"), strings.NewReader(requestBody))
		req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)

		// when
		rec := NewRequest(req).
			Handle(AdministratorController{}.Create)

		// then
		assert.Equal(t, http.StatusOK, rec.Code)
	})

	t.Run("Create Administrator - duplication BSTORE", func(t *testing.T) {
		// given
		requestBody := `{
			"email":"admin5@gmail.com",
			"password":"123456",
			"mobile":"01000000000",
			"roleType": "` + enum.BSTORE.String() + `"
		}`
		req := httptest.NewRequest(echo.POST, fmt.Sprintf("/api/admin/administrators"), strings.NewReader(requestBody))
		req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)

		// when
		rec := NewRequest(req).
			Handle(AdministratorController{}.Create)

		// then
		assert.Equal(t, http.StatusBadRequest, rec.Code)

		result := responseDto.ApiError{}
		json.Unmarshal(rec.Body.Bytes(), &result)

		assert.Equal(t, 10004, result.Code)
		assert.Equal(t, "이미 등록되었습니다.", result.Message)
	})
}

func TestAdministratorController_Update(t *testing.T) {
	DatabaseFixture{}.setUpDefault(xormDb.Engine)

	t.Run("Update Administrator - BSTORE", func(t *testing.T) {
		// given
		adminId := "1"
		requestBody := `{
			"id":` + adminId + `,
			"email":"admin4@gmail.com",
			"password":"123456",
			"mobile":"01000000000",
			"roleType": "` + enum.BSTORE.String() + `"
		}`
		req := httptest.NewRequest(echo.POST, fmt.Sprintf("/api/admin/administrators/:id"), strings.NewReader(requestBody))
		req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)
		userClaim := auth.UserClaim{
			Id:    1,
			Roles: "admin",
		}

		// when
		rec := NewRequest(req).
			WithUser(&userClaim).
			WithParam("id", adminId).
			Handle(AdministratorController{}.Update)

		// then
		assert.Equal(t, http.StatusOK, rec.Code)
	})

	t.Run("Update Administrator - nothing id", func(t *testing.T) {
		// given
		adminId := "100"
		requestBody := `{
			"id":` + adminId + `,
			"email":"bstore@gmail.com",
			"password":"123456",
			"mobile":"01000000000",
			"roleType": "` + enum.BSTORE.String() + `"
		}`
		req := httptest.NewRequest(echo.POST, fmt.Sprintf("/api/admin/administrators/:id"), strings.NewReader(requestBody))
		req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)
		userClaim := auth.UserClaim{
			Id:    1,
			Roles: "admin",
		}

		// when
		rec := NewRequest(req).
			WithUser(&userClaim).
			WithParam("id", adminId).
			Handle(AdministratorController{}.Update)

		// then
		assert.Equal(t, http.StatusNotFound, rec.Code)

		result := errors.ErrorResponseWrapper{}
		json.Unmarshal(rec.Body.Bytes(), &result)

		assert.Equal(t, 10003, result.Code)
		assert.Equal(t, "결과를 찾을 수 없습니다.", result.Message)
	})

	t.Run("Update Administrator - duplication", func(t *testing.T) {
		// given
		adminId := "1"
		requestBody := `{
			"id":` + adminId + `,
			"email":"bstore@gmail.com",
			"password":"123456",
			"mobile":"01000000000",
			"roleType": "` + enum.BSTORE.String() + `"
		}`
		req := httptest.NewRequest(echo.POST, fmt.Sprintf("/api/admin/administrators/:id"), strings.NewReader(requestBody))
		req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)
		userClaim := auth.UserClaim{
			Id:    1,
			Roles: "admin",
		}

		// when
		rec := NewRequest(req).
			WithUser(&userClaim).
			WithParam("id", adminId).
			Handle(AdministratorController{}.Update)

		// then
		assert.Equal(t, http.StatusBadRequest, rec.Code)

		result := responseDto.ApiError{}
		json.Unmarshal(rec.Body.Bytes(), &result)

		assert.Equal(t, 10004, result.Code)
		assert.Equal(t, "이미 등록되었습니다.", result.Message)
	})
}
