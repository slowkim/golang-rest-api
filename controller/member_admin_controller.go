package controller

import (
	"golang-rest-api/common/errors"
	"golang-rest-api/config/handler"
	requestDto "golang-rest-api/dto/request"
	"golang-rest-api/member/service"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"
)

type MemberAdminController struct {
}

func (controller MemberAdminController) Init(g *echo.Group) {
	g.GET("/:id", controller.GetMember, handler.CheckAuthMiddleware([]string{"admin"}))
	g.GET("", controller.GetMembers, handler.CheckAuthMiddleware([]string{"admin"}))
}

func (MemberAdminController) GetMember(ctx echo.Context) (err error) {
	memberId, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		return errors.ApiParamValidError(err)
	}

	memberSummary, err := service.MemberService().GetMember(ctx.Request().Context(), memberId)
	if err != nil {
		log.Errorf("GetMember Error:  %s", err.Error())
		return err
	}

	return ctx.JSON(http.StatusOK, memberSummary)
}

func (MemberAdminController) GetMembers(ctx echo.Context) error {
	pageable := requestDto.GetPageableFromRequest(ctx)

	searchMemberQueryParams := requestDto.SearchMemberQueryParams{}

	if err := ctx.Bind(&searchMemberQueryParams); err != nil {
		return errors.ApiParamValidError(err)
	}

	pageResult, err := service.MemberService().GetMembers(ctx.Request().Context(), searchMemberQueryParams, pageable)
	if err != nil {
		log.Errorf("GetMembers Error:  %s", err.Error())
		return err
	}

	return ctx.JSON(http.StatusOK, pageResult)
}
