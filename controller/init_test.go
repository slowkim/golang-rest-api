package controller

import (
	"context"
	"encoding/json"
	"golang-rest-api/config"
	auth "golang-rest-api/config/authentication"
	"golang-rest-api/config/handler"
	"net/http"
	"net/http/httptest"
	"os"
	"runtime"

	"github.com/labstack/echo/v4"
	_ "github.com/mattn/go-sqlite3"
)

var (
	echoApp      *echo.Echo
	xormDb       config.DatabaseWrapper
	xormMemberDb config.DatabaseWrapper
)

func init() {
	runtime.GOMAXPROCS(1)
	os.Setenv("CONFIGOR_ENV", "test")
	os.Setenv("GOLANG_REST_API_ENCRYPT_KEY", "UjXn2r5u8x/A?D(G+KbPeSgVkYp3s6v9")
	config.ConfigureEnvironment("../", "GOLANG_REST_API_ENCRYPT_KEY", "GOLANG_REST_API_AWS_S3_KEY", "KAKAO_MESSAGING_ACCOUNT_PASSWORD")
	xormDb = config.ConfigureDatabase()
	xormMemberDb = config.ConfigureDatabase()
	config.ConfigureLogger()

	e := config.ConfigureEcho()

	e.Use(handler.InitUserClaimMiddleware())
	e.HTTPErrorHandler = handler.CustomHTTPErrorHandler

	echoApp = e
}

type EchoContextBuilder struct {
	request     *http.Request
	paramKeys   []string
	paramValues []string
	context     map[string]interface{}
}

func NewRequest(req *http.Request) EchoContextBuilder {
	return EchoContextBuilder{request: req, paramKeys: make([]string, 0), paramValues: make([]string, 0), context: make(map[string]interface{})}
}

func (cb EchoContextBuilder) WithContext(key string, value interface{}) EchoContextBuilder {
	cb.context[key] = value
	return cb
}

func (cb EchoContextBuilder) WithUser(user *auth.UserClaim) EchoContextBuilder {
	return cb.WithContext(config.ContextUserClaimKey, user)
}

func (cb EchoContextBuilder) WithParam(key string, value string) EchoContextBuilder {
	cb.paramKeys = append(cb.paramKeys, key)
	cb.paramValues = append(cb.paramValues, value)
	return cb
}

func (cb EchoContextBuilder) Handle(handlerFunc echo.HandlerFunc) *httptest.ResponseRecorder {
	rec := httptest.NewRecorder()
	echo := echoApp.NewContext(cb.request, rec)

	echo.SetParamNames(cb.paramKeys...)
	echo.SetParamValues(cb.paramValues...)

	// DB Context는 기본 설정으로 간주.
	ctx := context.WithValue(echo.Request().Context(), config.ContextDBKey, xormDb)
	ctx = context.WithValue(echo.Request().Context(), config.ContextMemberDBKey, xormMemberDb)

	for k, v := range cb.context {
		ctx = context.WithValue(echo.Request().Context(), k, v)
	}

	echo.SetRequest(echo.Request().WithContext(ctx))

	session := handler.CreateDatabaseContext(xormDb, config.ContextDBKey)
	memberSession := handler.CreateDatabaseContext(xormMemberDb, config.ContextMemberDBKey)
	err := session(memberSession(handlerFunc))(echo)
	if err != nil {
		echo.Error(err)
	}
	return rec
}

func jsonResponse(rec *httptest.ResponseRecorder) (result map[string]interface{}, err error) {
	str := rec.Body.String()
	result = make(map[string]interface{})
	err = json.Unmarshal([]byte(str), &result)
	if err != nil {
		return
	}
	return
}
