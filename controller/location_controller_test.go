package controller

import (
	"encoding/json"
	"fmt"
	auth "golang-rest-api/config/authentication"
	responseDto "golang-rest-api/dto/response"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

func Test_LocationController(t *testing.T) {
	DatabaseFixture{}.setUpDefault(xormDb.Engine)

	t.Run("address", func(t *testing.T) {
		// given
		query := url.QueryEscape("안국역 3호선")

		req := httptest.NewRequest(echo.GET, fmt.Sprintf("/api/locations/address?query=%s", query), nil)
		req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)
		userClaim := auth.UserClaim{
			Id:    1,
			Roles: "member",
		}

		// when
		rec := NewRequest(req).
			WithUser(&userClaim).
			Handle(LocationController{}.SearchAddress)

		// then
		assert.Equal(t, http.StatusOK, rec.Code)
		result := make([]responseDto.Address, 0)
		json.Unmarshal(rec.Body.Bytes(), &result)

		assert.NotEmpty(t, result[0].AddressName)
	})
}
