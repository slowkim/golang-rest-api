package controller

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	auth "golang-rest-api/config/authentication"
	responseDto "golang-rest-api/dto/response"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

func TestMemberController_Get(t *testing.T) {
	DatabaseFixture{}.setUpDefault(xormDb.Engine)

	t.Run("GetMember", func(t *testing.T) {
		// given
		req := httptest.NewRequest(echo.GET, fmt.Sprintf("/api/members"), nil)
		req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)
		userClaim := auth.UserClaim{
			Id:    1,
			Roles: "member",
		}

		// when
		rec := NewRequest(req).
			WithUser(&userClaim).
			Handle(MemberController{}.GetMember)

		// then
		assert.Equal(t, http.StatusOK, rec.Code)
		result := responseDto.MemberSummary{}
		json.Unmarshal(rec.Body.Bytes(), &result)

		assert.Equal(t, "hahaha", result.Nickname)
	})
}
