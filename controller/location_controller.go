package controller

import (
	"golang-rest-api/adapter"
	"golang-rest-api/common/errors"
	"golang-rest-api/config/handler"
	requestDto "golang-rest-api/dto/request"
	"net/http"

	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"
)

type LocationController struct {
}

func (controller LocationController) Init(g *echo.Group) {
	g.GET("/places", controller.SearchPlaces, handler.CheckAuthMiddleware([]string{"*"}))
	g.GET("/coord-to-address", controller.CoordToAddress, handler.CheckAuthMiddleware([]string{"*"}))
	g.GET("/address", controller.SearchAddress, handler.CheckAuthMiddleware([]string{"*"}))
}

func (controller LocationController) SearchPlaces(ctx echo.Context) error {
	query := ctx.QueryParam("query")
	if len(query) == 0 {
		return errors.ErrNotValid
		//return errors.ApiParamValidError(errors.New("query parameter is required."))
	}

	addressWithPositionList, err := adapter.KakaoAdapter().SearchPlaces(query)
	if err != nil {
		log.Errorf("SearchAddress Error:  %s", err.Error())
		return err
	}

	return ctx.JSON(http.StatusOK, addressWithPositionList)
}

func (controller LocationController) CoordToAddress(ctx echo.Context) error {
	latitude := ctx.QueryParam("latitude")
	longitude := ctx.QueryParam("longitude")
	if len(latitude) == 0 {
		//return errors.ApiParamValidError(errors.New("latitude parameter is required."))
		return errors.ErrNotValid
	}
	if len(longitude) == 0 {
		//return errors.ApiParamValidError(errors.New("longitude parameter is required."))
		return errors.ErrNotValid
	}

	address, err := adapter.KakaoAdapter().CoordToAddress(latitude, longitude)
	if err != nil {
		log.Errorf("CoordToAddress Error:  %s", err.Error())
		return err
	}

	result := map[string]string{}
	result["address"] = address

	return ctx.JSON(http.StatusOK, result)
}

func (controller LocationController) SearchAddress(ctx echo.Context) error {
	query := ctx.QueryParam("query")
	if len(query) == 0 {
		return errors.ErrNotValid
		//return errors.ApiParamValidError(errors.New("query parameter is required."))
	}

	pageable := requestDto.GetPageableFromRequest(ctx)

	addressList, err := adapter.JusoAdapter().SearchAddress(query, pageable)
	if err != nil {
		log.Errorf("SearchAddress Error:  %s", err.Error())
		return err
	}

	return ctx.JSON(http.StatusOK, addressList)
}
