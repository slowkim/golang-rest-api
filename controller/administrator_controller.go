package controller

import (
	"golang-rest-api/administrator/service"
	"golang-rest-api/common/errors"
	"golang-rest-api/config/handler"
	requestDto "golang-rest-api/dto/request"
	responseDto "golang-rest-api/dto/response"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"
)

type AdministratorController struct {
}

func (controller AdministratorController) Init(g *echo.Group) {
	g.POST("", controller.Create, handler.CheckAuthMiddleware([]string{"admin"}))
	g.PUT("/:id", controller.Update, handler.CheckAuthMiddleware([]string{"admin"}))
	g.GET("/:id", controller.GetAdministrator, handler.CheckAuthMiddleware([]string{"admin"}))
	g.GET("", controller.GetAdministrators, handler.CheckAuthMiddleware([]string{"admin"}))
}

// @Summary Create AdministratorCreate
// @Description Create Administrator
// @Accept json
// @Produce json
// @Param AdministratorCreate body requestDto.AdministratorCreate true "Administrator Create"
// @Success 200
// @Router /api/admin/administrators [post]
func (AdministratorController) Create(ctx echo.Context) error {
	administratorCreate := requestDto.AdministratorCreate{}

	if err := ctx.Bind(&administratorCreate); err != nil {
		return errors.ApiParamValidError(err)
	}

	if err := administratorCreate.Validate(ctx); err != nil {
		return err
	}

	err := service.AdministratorService().Create(ctx.Request().Context(), administratorCreate)
	if err != nil {
		log.Errorf("Create Error:  %s", err.Error())
		return err
	}

	return ctx.NoContent(http.StatusOK)
}

// @Summary Update AdministratorUpdate
// @Description Update Administrator
// @Accept json
// @Produce json
// @Param id path int true "Administrator ID"
// @Param AdministratorUpdate body requestDto.AdministratorUpdate true "Administrator Update"
// @Success 200
// @Router /api/admin/administrators [put]
func (AdministratorController) Update(ctx echo.Context) error {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		return errors.ApiParamValidError(err)
	}

	administratorUpdate := requestDto.AdministratorUpdate{}

	if err := ctx.Bind(&administratorUpdate); err != nil {
		return errors.ApiParamValidError(err)
	}

	if err := administratorUpdate.Validate(ctx); err != nil {
		return err
	}

	if id != administratorUpdate.Id {
		return errors.ErrIdInconsistent
	}

	err = service.AdministratorService().Update(ctx.Request().Context(), administratorUpdate)
	if err != nil {
		log.Errorf("Update Error:  %s", err.Error())
		return err
	}

	return ctx.NoContent(http.StatusOK)
}

// @Summary Get Administrator
// @Description Get Administrator
// @Accept json
// @Produce json
// @Param id path int true "Administrator ID"
// @Success 200 {object} responseDto.AdministratorSummary
// @Router /api/admin/administrators/{id} [Get]
func (AdministratorController) GetAdministrator(ctx echo.Context) error {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		return errors.ApiParamValidError(err)
	}
	administrator := responseDto.AdministratorSummary{}
	administrator, err = service.AdministratorService().GetAdministrator(ctx.Request().Context(), id)
	if err != nil {
		log.Errorf("GetAdministrator Error:  %s", err.Error())
		return err
	}

	return ctx.JSON(http.StatusOK, administrator)
}

// @Summary Get Administrators
// @Description Get Administrators
// @Accept json
// @Produce json
// @Param page path int false "page"
// @Param pageSize path int false "pageSize"
// @Param SearchAdminQueryParams query requestDto.SearchAdminQueryParams true "SearchAdmin QueryParams"
// @Success 200 {object} []responseDto.AdministratorSummary
// @Router /api/admin/administrators [Get]
func (AdministratorController) GetAdministrators(ctx echo.Context) error {
	pageable := requestDto.GetPageableFromRequest(ctx)

	searchAdminQueryParams := requestDto.SearchAdminQueryParams{}

	if err := ctx.Bind(&searchAdminQueryParams); err != nil {
		return errors.ApiParamValidError(err)
	}

	pageResult := responseDto.PageResult{}

	pageResult, err := service.AdministratorService().GetAdministrators(ctx.Request().Context(), searchAdminQueryParams, pageable)
	if err != nil {
		log.Errorf("GetAdministrators Error:  %s", err.Error())
		return err
	}

	return ctx.JSON(http.StatusOK, pageResult)
}
