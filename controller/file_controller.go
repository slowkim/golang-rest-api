package controller

import (
	"fmt"
	"golang-rest-api/adapter"
	"golang-rest-api/common"
	"golang-rest-api/common/errors"
	"golang-rest-api/config/handler"
	"net/http"
	"net/url"
	"time"

	"github.com/labstack/echo/v4"
)

type FileController struct {
}

func (controller FileController) Init(g *echo.Group) {
	g.POST("", controller.CreateFileUpload, handler.CheckAuthMiddleware([]string{"*"}))
}

func (FileController) CreateFileUpload(ctx echo.Context) error {
	path := ctx.FormValue("path")

	form, err := ctx.MultipartForm()
	if err != nil {
		return err
	}

	files := form.File["files"]

	uploadPath := fmt.Sprintf("%s/%v", path, time.Now().Format(common.DateLayout8))

	if files == nil || len(files) == 0 {
		return errors.ErrNotValid
	}

	fileUrls := make([]string, len(files))

	for i, file := range files {
		src, err := file.Open()
		if err != nil {
			return err
		}
		defer src.Close()
		accessUrl, err := adapter.AwsS3Adapter().UploadFile(uploadPath, src, file)

		if err != nil {
			return errors.Throw(err)
		}

		_, err = url.ParseRequestURI(accessUrl)
		if err != nil {
			//return errors.ApiParamValidError(errors.New("incorrect file url"))
			return errors.ErrNotValid
		}

		fileUrls[i] = accessUrl
	}

	resp := make(map[string][]string)
	resp["accessUrl"] = fileUrls

	return ctx.JSON(http.StatusOK, resp)
}
