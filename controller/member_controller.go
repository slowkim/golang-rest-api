package controller

import (
	"golang-rest-api/common"
	"golang-rest-api/config/handler"
	"golang-rest-api/member/service"
	"net/http"

	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"
)

type MemberController struct {
}

func (controller MemberController) Init(g *echo.Group) {
	g.GET("", controller.GetMember, handler.CheckAuthMiddleware([]string{"member"}))
}

func (MemberController) GetMember(ctx echo.Context) (err error) {
	userClaim := common.GetUserClaim(ctx.Request().Context())

	member, err := service.MemberService().GetMember(ctx.Request().Context(), userClaim.Id)
	if err != nil {
		log.Errorf("GetMember Error:  %s", err.Error())
		return err
	}

	return ctx.JSON(http.StatusOK, member)
}
