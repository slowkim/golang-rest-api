FROM golang:1.16 AS builder
WORKDIR /go/src/golang-rest-api
COPY . .
# disable cgo
ENV CGO_ENABLED=0
RUN go mod download
RUN go install

# make application docker image use alpine
FROM alpine:3.10
# using timezone
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Asia/Seoul
RUN apk add -U tzdata
WORKDIR /go/bin/
# copy config files to image
COPY --from=builder /go/src/golang-rest-api/config/*.json ./config/
# copy execute file to image
COPY --from=builder /go/bin/golang-rest-api .
EXPOSE 9006
CMD ["./golang-rest-api"]
