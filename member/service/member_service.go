package service

import (
	"context"
	requestDto "golang-rest-api/dto/request"
	responseDto "golang-rest-api/dto/response"
	"golang-rest-api/member/entity"
	"golang-rest-api/member/mapper"
	"golang-rest-api/member/repository"
	"sync"
)

var (
	memberServiceOnce     sync.Once
	memberServiceInstance *memberService
)

func MemberService() *memberService {
	memberServiceOnce.Do(func() {
		memberServiceInstance = &memberService{}
	})

	return memberServiceInstance
}

type memberService struct {
}

func (memberService) GetMember(ctx context.Context, memberId int64) (memberSummary responseDto.MemberSummary, err error) {
	member, err := repository.MemberRepository().FindById(ctx, memberId)
	if err != nil {
		return
	}

	memberSummary = mapper.MakeMemberSummary(member)

	return
}

func (memberService) GetMemberByKakaoId(ctx context.Context, kakaoId int64) (memberSummary responseDto.MemberSummary, err error) {
	member, err := repository.MemberRepository().FindByKakaoId(ctx, kakaoId)
	if err != nil {
		return
	}

	memberSummary = mapper.MakeMemberSummary(member)

	return
}

func (memberService) Create(ctx context.Context, member *entity.Member) (int64, error) {
	return repository.MemberRepository().Create(ctx, member)
}

func (memberService) Update(ctx context.Context, member *entity.Member) (err error) {
	return repository.MemberRepository().Update(ctx, member)
}

func (memberService) GetMembers(ctx context.Context, searchMemberQueryParams requestDto.SearchMemberQueryParams, pageable requestDto.Pageable) (results responseDto.PageResult, err error) {
	members, totalCount, err := repository.MemberRepository().FindAll(ctx, searchMemberQueryParams, pageable)
	if err != nil {
		return
	}

	memberSummaries := mapper.MakeMemberSummaries(members)

	results = responseDto.PageResult{
		Result:     memberSummaries,
		TotalCount: totalCount,
	}

	return
}
