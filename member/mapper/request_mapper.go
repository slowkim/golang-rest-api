package mapper

import (
	"golang-rest-api/common"
	"golang-rest-api/member/entity"
	"time"
)

func NewMemberForKakao(userInfo map[string]interface{}) entity.Member {
	now := time.Now()

	profileImage := ""
	nickname := ""
	if properties, ok := userInfo["properties"]; ok {
		if properties.(map[string]interface{})["profile_image"] != nil {
			profileImage = properties.(map[string]interface{})["profile_image"].(string)
		}
		if properties.(map[string]interface{})["nickname"] != nil {
			nickname = properties.(map[string]interface{})["nickname"].(string)
		}
	}

	email := ""
	phoneNumber := ""
	if kakaoAccount, ok := userInfo["kakao_account"]; ok {
		if kakaoAccount.(map[string]interface{})["email"] != nil {
			email = kakaoAccount.(map[string]interface{})["email"].(string)
		}
		if kakaoAccount.(map[string]interface{})["phone_number"] != nil {
			phoneNumber = common.GetMobileNo(kakaoAccount.(map[string]interface{})["phone_number"].(string))
		}
	}

	return entity.Member{
		KakaoId:      int64(userInfo["id"].(float64)),
		Nickname:     nickname,
		ProfileImage: profileImage,
		Email:        email,
		Mobile:       common.SetEncrypt(phoneNumber),
		CreatedAt:    now,
		UpdatedAt:    now,
	}
}

func UpdateMemberForKakao(userInfo map[string]interface{}, m *entity.Member) {
	profileImage := ""
	nickname := ""
	if properties, ok := userInfo["properties"]; ok {
		if properties.(map[string]interface{})["profile_image"] != nil {
			profileImage = properties.(map[string]interface{})["profile_image"].(string)
		}
		if properties.(map[string]interface{})["nickname"] != nil {
			nickname = properties.(map[string]interface{})["nickname"].(string)
		}
	}

	email := ""
	phoneNumber := ""
	if kakaoAccount, ok := userInfo["kakao_account"]; ok {
		if kakaoAccount.(map[string]interface{})["email"] != nil {
			email = kakaoAccount.(map[string]interface{})["email"].(string)
		}
		if kakaoAccount.(map[string]interface{})["phone_number"] != nil {
			phoneNumber = common.GetMobileNo(kakaoAccount.(map[string]interface{})["phone_number"].(string))
		}
	}

	m.Nickname = nickname
	m.ProfileImage = profileImage
	m.Email = email
	m.Mobile = common.SetEncrypt(phoneNumber)
}
