package mapper

import (
	"golang-rest-api/common"
	responseDto "golang-rest-api/dto/response"
	entity "golang-rest-api/member/entity"
)

func MakeMemberSummary(member entity.Member) (memberSummary responseDto.MemberSummary) {

	memberSummary = responseDto.MemberSummary{
		Id:              member.Id,
		Nickname:        member.Nickname,
		ProfileImageURL: member.ProfileImage,
		Mobile:          common.GetDecrypt(member.Mobile),
		Email:           member.Email,
		CreatedAt:       member.CreatedAt,
		UpdatedAt:       member.UpdatedAt,
	}

	return
}

func MakeMemberSummaries(members []entity.Member) (memberSummaries []responseDto.MemberSummary) {
	for _, member := range members {
		memberSummaries = append(memberSummaries, MakeMemberSummary(member))
	}

	return
}
