package repository

import (
	"context"
	"golang-rest-api/common"
	"golang-rest-api/common/errors"
	requestDto "golang-rest-api/dto/request"
	"golang-rest-api/member/entity"
	"sync"

	"github.com/go-xorm/xorm"
)

var (
	memberRepositoryOnce     sync.Once
	memberRepositoryInstance *memberRepository
)

func MemberRepository() *memberRepository {
	memberRepositoryOnce.Do(func() {
		memberRepositoryInstance = &memberRepository{}
	})

	return memberRepositoryInstance
}

type memberRepository struct {
}

func (memberRepository) FindById(ctx context.Context, id int64) (member entity.Member, err error) {
	session := common.GetMemberDB(ctx)
	member.Id = id

	has, err := session.Get(&member)
	if err != nil {
		return
	}

	if has == false {
		err = errors.ErrNoResult
		return
	}

	return
}

func (memberRepository) FindByKakaoId(ctx context.Context, kakaoId int64) (member entity.Member, err error) {
	session := common.GetMemberDB(ctx)
	member.KakaoId = kakaoId

	has, err := session.Get(&member)
	if err != nil {
		return
	}

	if has == false {
		return
	}

	return
}

func (memberRepository) Create(ctx context.Context, member *entity.Member) (memberId int64, err error) {
	session := common.GetMemberDB(ctx)
	memberId, err = session.Insert(member)
	//memberId := member.Id
	if err != nil {
		return
	}

	return
}

func (memberRepository) Update(ctx context.Context, member *entity.Member) (err error) {
	session := common.GetMemberDB(ctx).ID(member.Id)
	if _, err = session.AllCols().Update(member); err != nil {
		return
	}

	return
}

func (memberRepository) FindAll(ctx context.Context, searchMemberQueryParams requestDto.SearchMemberQueryParams, pageable requestDto.Pageable) (members []entity.Member, totalCount int64, err error) {
	queryBuilder := func() xorm.Interface {
		q := common.GetMemberDB(ctx).Table("members")
		q.Where("1=1")
		if searchMemberQueryParams.Nickname != "" {
			q.And("nickname = ? ", searchMemberQueryParams.Nickname)
		}
		if searchMemberQueryParams.Mobile != "" {
			q.And("mobile = ? ", common.SetEncrypt(searchMemberQueryParams.Mobile))
		}
		return q
	}

	var results []struct {
		entity.Member `xorm:"extends"`
	}

	if totalCount, err = queryBuilder().Limit(pageable.PageSize, pageable.Offset).Desc("members.id").FindAndCount(&results); err != nil {
		return
	}

	for _, result := range results {
		var member = entity.Member{}
		member = result.Member
		members = append(members, member)
	}

	return
}
