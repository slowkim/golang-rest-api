package entity

import (
	"time"
)

type Member struct {
	Id           int64     `xorm:"id pk autoincr" `
	KakaoId      int64     `xorm:"kakao_id notnull" `
	Nickname     string    `xorm:"nickname notnull" `
	ProfileImage string    `xorm:"profile_image notnull" `
	Email        string    `xorm:"email" `
	Mobile       string    `xorm:"mobile"`
	CreatedAt    time.Time `xorm:"created" `
	UpdatedAt    time.Time `xorm:"updated" `
}

func (Member) TableName() string {
	return "members"
}
