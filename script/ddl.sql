CREATE SCHEMA IF NOT EXISTS slowkim_dev DEFAULT CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;
USE slowkim_dev;

CREATE TABLE IF NOT EXISTS members (
  id INT NOT NULL NOT NULL AUTO_INCREMENT,
  kakao_id INT DEFAULT 0 NOT NULL,
  nickname VARCHAR(100) NOT NULL,
  profile_image VARCHAR(1000) NOT NULL,
  mobile VARCHAR(100) NOT NULL COMMENT '휴대전화',
  email VARCHAR(100) NOT NULL,
  created_at datetime NOT NULL,
  updated_at datetime NOT NULL,
  PRIMARY KEY (id),
  KEY idx_members_kakao_id (kakao_id),
  KEY idx_members_nickname (nickname),
  KEY idx_members_mobile (mobile),
  KEY idx_members_email (email)
);
CREATE TABLE IF NOT EXISTS administrators (
  id INT NOT NULL AUTO_INCREMENT,
  email VARCHAR(100) NOT NULL,
  password VARCHAR(100) NOT NULL,
  roles TEXT COMMENT 'roles',
  mobile VARCHAR(100) COMMENT '휴대전화',
  created_at datetime NOT NULL,
  updated_at datetime NOT NULL,
  PRIMARY KEY (id),
  KEY idx_administrators_email (email),
  KEY idx_administrators_mobile (mobile)
);
CREATE TABLE IF NOT EXISTS error_logs (
  id INT NOT NULL AUTO_INCREMENT,
  host_name VARCHAR(100) NOT NULL,
  service VARCHAR(100) NOT NULL,
  code VARCHAR(10) NOT NULL,
  error VARCHAR(1000) NOT NULL,
  stack_trace TEXT DEFAULT NULL,
  remote_ip VARCHAR(30) NOT NULL,
  host VARCHAR(100) NOT NULL,
  uri VARCHAR(1000) NOT NULL,
  method VARCHAR(100) NOT NULL,
  path VARCHAR(200) NOT NULL,
  referer VARCHAR(500) NOT NULL,
  user_agent VARCHAR(500) NOT NULL,
  status VARCHAR(100) NOT NULL,
  request_length INT NOT NULL,
  bytes_sent INT NOT NULL,
  body TEXT DEFAULT NULL,
  controller VARCHAR(100) NOT NULL,
  params TEXT DEFAULT NULL,
  action VARCHAR(1000) DEFAULT NULL,
  userId INT NOT NULL,
  auth_token VARCHAR(500) NOT NULL,
  timestamp datetime NOT NULL,
  created_at datetime NOT NULL,
  PRIMARY KEY (id),
  KEY idx_error_logs_service (service),
  KEY idx_error_logs_code (code),
  KEY idx_error_logs_method (method),
  KEY idx_error_logs_status (status),
  KEY idx_error_logs_timestamp (timestamp),
  KEY idx_error_logs_controller (controller)
);