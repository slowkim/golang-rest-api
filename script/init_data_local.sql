USE slowkim_dev;
INSERT INTO
  administrators (email,password,roles,mobile,created_at,updated_at)
VALUES
  ('slowkim@gmail.com','$2a$04$eBxWmaJMd3tLbo02Gkvwxun1mnIgKUFEPXewaDBWEtDmpe4x4hKeC','["admin"]','DxLjRCF26Tbk0rQ=',now(),now());

INSERT INTO members
(kakao_id,nickname,profile_image,mobile,email,created_at,updated_at)
VALUES
('1502851260','slowkim','http://k.kakaocdn.net/dn/b1RrbS/btqxJiaux0G/dbFkRaPTGNyIoKyOhe54Sk/img_640x640.jpg','DxLjTSt85Tvm1bQ=','DxLjRCF26Tbk0rQ=', now(), now());