package main

import (
	"golang-rest-api/adapter"
	"golang-rest-api/config"
	"golang-rest-api/config/handler"
	"golang-rest-api/controller"
	"net/http"

	_ "golang-rest-api/docs"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	echoSwagger "github.com/swaggo/echo-swagger"
)

// @title golang-rest-api Swagger API
// @version 1.0
// @BasePath /api/v1
func main() {
	config.ConfigureEnvironment("./", "JWT_SECRET", "GOLANG_REST_API_DB_PASSWORD", "GOLANG_REST_API_ENCRYPT_KEY", "GOLANG_REST_API_AWS_S3_KEY", "KAKAO_MESSAGING_ACCOUNT_PASSWORD")
	xormDb := config.ConfigureDatabase()
	xormMemberDb := config.ConfigureMemberDatabase()
	config.ConfigureLogger()

	eventMessagePublisher, err := adapter.NewKafkaMessagePublisher()
	if err != nil {
		logrus.Fatal("error set up EventMessagePublisher")
	} else {
		adapter.EventMessagePublisher = eventMessagePublisher
	}
	defer eventMessagePublisher.Close()

	eventMessageConsumer, err := adapter.NewConsumerGroup()
	if err != nil {
		logrus.Fatal("error set up EventMessageConsumer")
	} else {
		adapter.EventMessageConsumer = eventMessageConsumer
	}

	e := config.ConfigureEcho()

	e.GET("/", func(c echo.Context) error { return c.NoContent(http.StatusOK) })
	e.Use(handler.CreateDatabaseContext(xormDb, config.ContextDBKey))
	e.Use(handler.CreateDatabaseContext(xormMemberDb, config.ContextMemberDBKey))
	e.Use(handler.InitUserClaimMiddleware())
	e.HTTPErrorHandler = handler.CustomHTTPErrorHandler

	controller.FileController{}.Init(e.Group("/api/files"))

	controller.AdministratorController{}.Init(e.Group("/api/admin/administrators"))
	controller.AuthController{}.Init(e.Group("/api/auth"))
	controller.LocationController{}.Init(e.Group("/api/locations"))

	controller.MemberAdminController{}.Init(e.Group("/api/admin/members"))
	controller.MemberController{}.Init(e.Group("/api/members"))

	e.GET("/swagger/*", echoSwagger.WrapHandler)

	logrus.Info("Sharing Platform Service Server Started: Port=" + config.Config.HttpPort)
	//e.Start(":" + config.Config.HttpPort)
	e.Logger.Fatal(e.Start(":" + config.Config.HttpPort))
}
