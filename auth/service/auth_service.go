package service

import (
	"context"
	"golang-rest-api/adapter"
	adminService "golang-rest-api/administrator/service"
	auth "golang-rest-api/config/authentication"
	requestDto "golang-rest-api/dto/request"
	responseDto "golang-rest-api/dto/response"
	memberEntity "golang-rest-api/member/entity"
	memberMapper "golang-rest-api/member/mapper"
	memberRepository "golang-rest-api/member/repository"
	memberService "golang-rest-api/member/service"
	"time"

	"sync"
)

var (
	authServiceOnce     sync.Once
	authServiceInstance *authService
)

func AuthService() *authService {
	authServiceOnce.Do(func() {
		authServiceInstance = &authService{}
	})
	return authServiceInstance
}

type authService struct {
}

func (this authService) NewMemberJwtToken(ctx context.Context, authorizeCode string) (isSignUp bool, token string, err error) {
	isSignUp, member, err := this.AuthWithKakao(ctx, authorizeCode)
	if err != nil {
		return false, "", err
	}

	token, err = auth.CreateToken(member.Id, member.Nickname, "member")

	return
}

func (authService) NewAdminJwtToken(ctx context.Context, signIn requestDto.AdminSignIn) (token responseDto.JwtToken, err error) {
	// 인증은 기본적인 지연시간이 필요.
	time.Sleep(3 * time.Second)
	administrator, err := adminService.AdministratorService().AuthAdminWithEmailAndPassword(ctx, signIn)
	if err != nil {
		return
	}

	jwt, err := auth.CreateToken(administrator.Id, "", administrator.Roles...)

	token = responseDto.JwtToken{Token: jwt}
	return
}

func (authService) AuthWithKakao(ctx context.Context, authorizeCode string) (isSignUp bool, member memberEntity.Member, err error) {
	userInfo, err := adapter.KakaoAdapter().GetKakaoUserInfo(authorizeCode)
	if err != nil {
		return
	}

	findMember, err := memberRepository.MemberRepository().FindByKakaoId(ctx, int64(userInfo["id"].(float64)))
	if err != nil {
		return
	}

	if findMember.Id != int64(0) {
		memberMapper.UpdateMemberForKakao(userInfo, &findMember)
		if err = memberService.MemberService().Update(ctx, &findMember); err != nil {
			return
		}

		isSignUp = false
		member = findMember
		return
	}

	newMember := memberMapper.NewMemberForKakao(userInfo)
	if _, err = memberService.MemberService().Create(ctx, &newMember); err != nil {
		return
	}

	isSignUp = true
	member = newMember
	return
}
