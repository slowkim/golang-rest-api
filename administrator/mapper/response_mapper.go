package mapper

import (
	"golang-rest-api/administrator/entity"
	"golang-rest-api/common"
	responseDto "golang-rest-api/dto/response"
)

func MakeAdministratorSummary(administrator entity.Administrator) (administratorSummary responseDto.AdministratorSummary) {

	administratorSummary = responseDto.AdministratorSummary{
		Id:        administrator.Id,
		Email:     administrator.Email,
		Mobile:    common.GetDecrypt(administrator.Mobile),
		Roles:     administrator.Roles,
		CreatedAt: administrator.CreatedAt,
		UpdatedAt: administrator.UpdatedAt,
	}

	return
}

func MakeAdministratorSummaries(administrators []entity.Administrator) (administratorSummaries []responseDto.AdministratorSummary) {
	for _, administrator := range administrators {
		administratorSummaries = append(administratorSummaries, MakeAdministratorSummary(administrator))
	}

	return
}
