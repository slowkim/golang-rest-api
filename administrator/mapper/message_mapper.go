package mapper

import (
	"golang-rest-api/administrator/entity"
	"golang-rest-api/common"
	responseDto "golang-rest-api/dto/response"
)

func NewPublishMessageAdministrator(administrator entity.Administrator) (administratorSummary responseDto.AdministratorSummary) {
	administratorSummary = responseDto.AdministratorSummary{
		Id:        administrator.Id,
		Email:     administrator.Email,
		Mobile:    common.GetDecrypt(administrator.Mobile),
		Roles:     administrator.Roles,
		CreatedAt: administrator.CreatedAt,
		UpdatedAt: administrator.UpdatedAt,
	}

	return
}
