package mapper

import (
	administratorEntity "golang-rest-api/administrator/entity"
	"golang-rest-api/common"
	requestDto "golang-rest-api/dto/request"
	"strings"
)

func NewAdministrator(creation requestDto.AdministratorCreate) (administrator administratorEntity.Administrator, err error) {
	password, err := common.HashAndSalt(creation.Password)
	if err != nil {
		return
	}

	roles := []string{}
	roles = append(roles, creation.RoleType.String())
	administrator = administratorEntity.Administrator{
		Mobile:   common.SetEncrypt(strings.Trim(creation.Mobile, " ")),
		Password: password,
		Email:    creation.Email,
		Roles:    roles,
	}

	return
}

func UpdateAdministrator(edition requestDto.AdministratorUpdate, administrator *administratorEntity.Administrator) (err error) {
	password, err := common.HashAndSalt(edition.Password)
	if err != nil {
		return
	}

	roles := []string{}
	roles = append(roles, edition.RoleType.String())
	administrator.Email = edition.Email
	administrator.Password = password
	administrator.Mobile = common.SetEncrypt(strings.Trim(edition.Mobile, " "))
	administrator.Roles = roles

	return
}
