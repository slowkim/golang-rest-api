package service

import (
	"context"
	"fmt"
	"golang-rest-api/adapter"
	"golang-rest-api/administrator/entity"
	"golang-rest-api/administrator/mapper"
	"golang-rest-api/administrator/repository"
	"golang-rest-api/common/errors"
	requestDto "golang-rest-api/dto/request"
	responseDto "golang-rest-api/dto/response"
	"strconv"
	"sync"

	"github.com/sirupsen/logrus"
)

var (
	administratorServiceOnce     sync.Once
	administratorServiceInstance *administratorService
)

func AdministratorService() *administratorService {
	administratorServiceOnce.Do(func() {
		administratorServiceInstance = &administratorService{}
	})
	return administratorServiceInstance
}

type administratorService struct {
}

func (administratorService) Create(ctx context.Context, creation requestDto.AdministratorCreate) (err error) {
	_, err = repository.AdministratorRepository().FindByEmail(ctx, creation.Email)
	if err != nil && err != errors.ErrNoResult {
		return
	}

	if err != errors.ErrNoResult {
		err = errors.ErrDuplicationRegister
		return
	}

	newAdministrator, err := mapper.NewAdministrator(creation)
	if err != nil {
		return err
	}

	_, err = repository.AdministratorRepository().Create(ctx, &newAdministrator)
	if err != nil {
		return
	}

	go func() {
		actionType := "AdministratorCreate"
		PublishMessagesAdministrator(ctx, actionType, newAdministrator)
	}()

	return
}

func PublishMessagesAdministrator(ctx context.Context, actionType string, newAdministrator entity.Administrator) {
	eventMessage := adapter.TranslateKafkaSendMessage(ctx, actionType, newAdministrator)
	logrus.Println(eventMessage)
	if eventMessage != nil {
		key := strconv.FormatInt(newAdministrator.Id, 10)
		if err := adapter.EventMessagePublisher.Publish(eventMessage, key); err != nil {
			logrus.WithError(err).Error(fmt.Sprintf(`Administrator Create Id:%v - publish to message borker`, newAdministrator.Id))
		}
	}
}

func (administratorService) Update(ctx context.Context, edition requestDto.AdministratorUpdate) (err error) {
	administrator, err := repository.AdministratorRepository().FindById(ctx, edition.Id)
	if err != nil {
		return err
	}

	if administrator.Email != edition.Email {
		_, err = repository.AdministratorRepository().FindByEmail(ctx, edition.Email)
		if err != nil && err != errors.ErrNoResult {
			return
		}

		if err != errors.ErrNoResult {
			err = errors.ErrDuplicationRegister
			return
		}
	}
	err = mapper.UpdateAdministrator(edition, &administrator)
	if err != nil {
		return
	}

	err = repository.AdministratorRepository().Update(ctx, &administrator)
	if err != nil {
		return
	}

	return
}

func (administratorService) GetAdministrator(ctx context.Context, id int64) (administratorSummary responseDto.AdministratorSummary, err error) {
	administrator, err := repository.AdministratorRepository().FindById(ctx, id)
	if err != nil {
		return
	}

	administratorSummary = mapper.MakeAdministratorSummary(administrator)

	return
}

func (administratorService) GetAdministrators(ctx context.Context, searchQueryParams requestDto.SearchAdminQueryParams, pageable requestDto.Pageable) (results responseDto.PageResult, err error) {
	administrators, totalCount, err := repository.AdministratorRepository().FindAll(ctx, searchQueryParams, pageable)
	if err != nil {
		return
	}

	administratorSummaries := mapper.MakeAdministratorSummaries(administrators)

	results = responseDto.PageResult{
		Result:     administratorSummaries,
		TotalCount: totalCount,
	}

	return
}

func (administratorService) AuthAdminWithEmailAndPassword(ctx context.Context, signIn requestDto.AdminSignIn) (administrator entity.Administrator, err error) {
	administrator, err = repository.AdministratorRepository().FindByEmail(ctx, signIn.Email)
	if err != nil {
		if err == errors.ErrNoResult {
			err = errors.ErrNotValid
		}
		return
	}

	if err = administrator.ValidatePassword(signIn.Password); err != nil {
		return
	}

	return
}
