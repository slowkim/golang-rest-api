package enum

import "fmt"

type AdminRoleTypeEnum string

const (
	ADMIN         AdminRoleTypeEnum = "admin"
	BSTOREMANAGER AdminRoleTypeEnum = "bstore_manager"
	BSTORE        AdminRoleTypeEnum = "bstore"
)

func (s AdminRoleTypeEnum) String() string {
	switch s {
	case ADMIN:
		return "admin"
	case BSTOREMANAGER:
		return "bstore_manager"
	case BSTORE:
		return "bstore"
	default:
		return fmt.Sprintf("%v", s)
	}
}
