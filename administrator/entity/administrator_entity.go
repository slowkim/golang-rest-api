package entity

import (
	"golang-rest-api/common"
	"golang-rest-api/common/errors"
	"time"
)

type Administrator struct {
	Id        int64     `xorm:"id pk autoincr" `
	Email     string    `xorm:"email" `
	Mobile    string    `xorm:"mobile" `
	Password  string    `xorm:"password"`
	Roles     []string  `xorm:"roles jsonb"`
	CreatedAt time.Time `xorm:"created" `
	UpdatedAt time.Time `xorm:"updated" `
}

func (Administrator) TableName() string {
	return "administrators"
}

func (u Administrator) ValidatePassword(password string) (err error) {
	if common.ComparePasswords(u.Password, password) {
		return err
	}
	err = errors.ErrNotValid
	return
}
