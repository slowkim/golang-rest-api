package repository

import (
	"context"
	"golang-rest-api/administrator/entity"
	"golang-rest-api/common"
	"golang-rest-api/common/errors"
	requestDto "golang-rest-api/dto/request"
	"sync"

	"github.com/go-xorm/xorm"
)

var (
	administratorRepositoryOnce     sync.Once
	administratorRepositoryInstance *administratorRepository
)

func AdministratorRepository() *administratorRepository {
	administratorRepositoryOnce.Do(func() {
		administratorRepositoryInstance = &administratorRepository{}
	})

	return administratorRepositoryInstance
}

type administratorRepository struct {
}

func (administratorRepository) Create(ctx context.Context, creation *entity.Administrator) (adminId int64, err error) {
	session := common.GetDB(ctx)
	adminId, err = session.Insert(creation)
	if err != nil {
		return
	}

	return
}

func (administratorRepository) Update(ctx context.Context, edition *entity.Administrator) (err error) {
	session := common.GetDB(ctx).ID(edition.Id)
	if _, err = session.Update(edition); err != nil {
		return
	}

	return
}

func (administratorRepository) FindById(ctx context.Context, id int64) (entity.Administrator, error) {
	session := common.GetDB(ctx)
	var adminUser = entity.Administrator{Id: id}
	has, err := session.Get(&adminUser)
	if err != nil {
		return adminUser, err
	}

	if has == false {
		return adminUser, errors.ErrNoResult
	}

	return adminUser, nil
}

func (administratorRepository) FindByEmail(ctx context.Context, email string) (administrator entity.Administrator, err error) {
	session := common.GetDB(ctx)
	administrator.Email = email

	has, err := session.Get(&administrator)
	if err != nil {
		return
	}

	if has == false {
		err = errors.ErrNoResult
		return
	}

	return
}

func (administratorRepository) FindAll(ctx context.Context, searchQueryParams requestDto.SearchAdminQueryParams, pageable requestDto.Pageable) (administrators []entity.Administrator, totalCount int64, err error) {
	queryBuilder := func() xorm.Interface {
		q := common.GetDB(ctx).Table("administrators")
		q.Where("1=1")
		if searchQueryParams.Email != "" {
			q.And("email = ? ", searchQueryParams.Email)
		}
		if searchQueryParams.Mobile != "" {
			q.And("mobile = ? ", common.SetEncrypt(searchQueryParams.Mobile))
		}
		return q
	}

	var results []struct {
		entity.Administrator `xorm:"extends"`
	}

	if totalCount, err = queryBuilder().Limit(pageable.PageSize, pageable.Offset).Desc("administrators.id").FindAndCount(&results); err != nil {
		return
	}

	for _, result := range results {
		var administrator = entity.Administrator{}
		administrator = result.Administrator
		administrators = append(administrators, administrator)
	}

	return
}
