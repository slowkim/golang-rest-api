module golang-rest-api

go 1.16

require (
	github.com/Shopify/sarama v1.29.1
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/aws/aws-sdk-go v1.38.68
	github.com/bettercode-oss/rest v0.0.4
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-playground/validator/v10 v10.6.1
	github.com/go-sql-driver/mysql v1.6.0
	github.com/go-xorm/xorm v0.7.9
	github.com/jinzhu/configor v1.2.1
	github.com/labstack/echo/v4 v4.3.0
	github.com/labstack/gommon v0.3.0
	github.com/mattn/go-sqlite3 v1.14.7
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	github.com/swaggo/echo-swagger v1.1.0
	github.com/swaggo/swag v1.7.0
	gitlab.com/slowkim/golang-kafka v0.0.0-20210617013501-f7e9703ea0b2
	golang.org/x/crypto v0.0.0-20210616213533-5ff15b29337e
	golang.org/x/net v0.0.0-20210614182718-04defd469f4e
	gopkg.in/testfixtures.v2 v2.6.0
	xorm.io/core v0.7.3
)
