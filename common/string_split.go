package common

import (
	"unicode/utf8"
)

func StringSplit(str string, len int) string {
	b := []byte(str)
	idx := 0
	for i := 0; i < len; i++ {
		_, size := utf8.DecodeLastRune(b[idx:])
		idx += size
	}
	return str[:idx]
}

func RuneStringSplit(str string, len int) string {
	runeStr := []rune(str)
	splicelen := len
	stringLen := utf8.RuneCountInString(str)
	if len > stringLen {
		splicelen = stringLen
	}
	result := string(runeStr[:splicelen])
	return result
}
