package common

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"golang-rest-api/config"
	"io"
)

var iv = []byte{35, 46, 57, 24, 85, 35, 24, 74, 87, 35, 88, 98, 66, 32, 14, 05}

func encodeBase64(b []byte) string {
	return base64.StdEncoding.EncodeToString(b)
}

func decodeBase64(s string) []byte {
	data, err := base64.StdEncoding.DecodeString(s)
	if err != nil {
		panic(err)
	}
	return data
}

func GetDecrypt(text string) string {
	key := config.Config.Encrypt.EncryptKey
	return Decrypt(key, text)
}

func SetEncrypt(text string) string {
	key := config.Config.Encrypt.EncryptKey
	return Encrypt(key, text)
}

func Encrypt(key, text string) string {
	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		panic(err)
	}
	plaintext := []byte(text)
	cfb := cipher.NewCFBEncrypter(block, iv)
	ciphertext := make([]byte, len(plaintext))
	cfb.XORKeyStream(ciphertext, plaintext)
	return encodeBase64(ciphertext)
}

func Decrypt(key, text string) string {
	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		panic(err)
	}
	ciphertext := decodeBase64(text)
	cfb := cipher.NewCFBEncrypter(block, iv)
	plaintext := make([]byte, len(ciphertext))
	cfb.XORKeyStream(plaintext, ciphertext)
	return string(plaintext)
}

func GetCfbDecrypter(text string) string {
	key := config.Config.Encrypt.EncryptKey
	return CfbDecrypter(key, text)
}

func SetCfbEncrypter(text string) string {
	key := config.Config.Encrypt.EncryptKey
	return CfbEncrypter(key, text)
}

func CfbEncrypter(key, text string) string {
	plaintext := []byte(text)

	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		panic(err.Error())
	}

	ciphertext := make([]byte, aes.BlockSize+len(plaintext))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		panic(err.Error())
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], plaintext)
	return fmt.Sprintf("%x", ciphertext)
}

func CfbDecrypter(key, text string) string {
	ciphertext, _ := hex.DecodeString(text)

	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		panic(err.Error())
	}

	if len(ciphertext) < aes.BlockSize {
		panic("암호화된 text가 넘 짧습니다")
	}

	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]
	stream := cipher.NewCFBDecrypter(block, iv)
	stream.XORKeyStream(ciphertext, ciphertext)
	// fmt.Printf("%s", ciphertext)
	return fmt.Sprintf("%s", ciphertext)
}
