package common

const (
	DateLayout6  = "060102"
	DateLayout8  = "20060102"
	DateLayout10 = "2006-01-02"
	DateLayout14 = "20060102150405"
	DateLayout19 = "2006-01-02 15:04:05"
)
