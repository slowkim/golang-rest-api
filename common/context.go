package common

import (
	"golang-rest-api/config"
	auth "golang-rest-api/config/authentication"

	"github.com/go-xorm/xorm"
	"golang.org/x/net/context"
)

func getContextValue(ctx context.Context, key string) (value interface{}, exist bool) {
	value = ctx.Value(key)
	exist = value != nil

	return
}

func GetUserClaim(ctx context.Context) *auth.UserClaim {
	value, exist := getContextValue(ctx, config.ContextUserClaimKey)

	if !exist {
		return nil
	}

	return value.(*auth.UserClaim)
}

func GetDB(ctx context.Context) *xorm.Session {
	value, exist := getContextValue(ctx, config.ContextDBKey)

	if !exist {
		panic("DB is not exist")
	}
	if db, ok := value.(*xorm.Session); ok {
		return db
	}
	if db, ok := value.(*xorm.Engine); ok {
		return db.NewSession()
	}
	panic("DB is not exist")
}

func GetMemberDB(ctx context.Context) *xorm.Session {
	value, exist := getContextValue(ctx, config.ContextMemberDBKey)

	if !exist {
		panic("MemberDB is not exist")
	}
	if db, ok := value.(*xorm.Session); ok {
		return db
	}
	if db, ok := value.(*xorm.Engine); ok {
		return db.NewSession()
	}
	panic("MemberDB is not exist")
}
