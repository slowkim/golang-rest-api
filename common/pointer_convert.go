package common

func ConvertPointerInt64ToInt64(pointInt64 *int64) int64 {
	if pointInt64 != nil {
		return *pointInt64
	}

	return int64(0)
}
