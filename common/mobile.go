package common

import "regexp"

func GetMobileNo(phoneNumber string) (mobileNo string) {
	m1 := regexp.MustCompile(`(\+\d{1,6}\D)|\D`) //ITU-T 권고 E.164
	replacePhoneNumber := m1.ReplaceAllString(phoneNumber, "")

	var validMobile1 = regexp.MustCompile(`^(1[0|1])\d{7,8}$`)
	matched1 := validMobile1.MatchString(replacePhoneNumber)
	if matched1 {
		mobileNo = "0" + replacePhoneNumber
	}

	var validMobile2 = regexp.MustCompile(`^(01[0|1])\d{7,8}$`)
	matched2 := validMobile2.MatchString(replacePhoneNumber)
	if matched2 {
		mobileNo = replacePhoneNumber
	}

	return
}
