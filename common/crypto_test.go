package common

import (
	"fmt"
	"testing"
)

func Test_Encrypt(t *testing.T) {

	key := "UjXn2r5u8x/A?D(G+KbPeSgVkYp3s6v9"
	plaintext1 := "01000000000"
	foo := Encrypt(key, plaintext1)
	fmt.Println(foo)
	// "aIqdvWfISKiKQSw="
	fmt.Println(Decrypt(key, "DxLjRSN17TPi1bc="))
}

func Test_CfbEncrypter(t *testing.T) {

	key := "UjXn2r5u8x/A?D(G+KbPeSgVkYp3s6v9"
	plaintext1 := "Razzaq Kirsten Zenub Ludvig"
	foo := CfbEncrypter(key, plaintext1)
	fmt.Println(foo)
	// "aIqdvWfISKiKQSw="
	fmt.Println(CfbDecrypter(key, foo))
}
