package config

import (
	"fmt"
	"os"

	"github.com/jinzhu/configor"
	"github.com/sirupsen/logrus"
)

const (
	ContextUserClaimKey = "userClaim"
	ContextDBKey        = "DB"
	ContextMemberDBKey  = "MemberDB"
)

var Config = struct {
	HttpPort string
	Database struct {
		Driver           string
		User             string
		Connection       string
		ConnectionString string
	}
	MemberDatabase struct {
		Driver           string
		User             string
		Connection       string
		ConnectionString string
	}
	Log struct {
		ShowSql bool
	}
	Encrypt struct {
		EncryptKey string
	}
	Kakao struct {
		RestApiKey  string
		RedirectURL string
	}
	Juso struct {
		JusoApiKey string
	}
	Cu struct {
		PostBoxURL  string
		CustNo      string
		ShowHttpLog bool
	}
	WebApp struct {
		LoginUrl string
		HomeUrl  string
	}
	AwsS3 struct {
		SecretAccessKey string
		Bucket          string
		Region          string
		AccessKeyId     string
		HttpEndPoint    string
	}
	Service struct {
		Name string
	}
	EventMessageBroker struct {
		Kafka struct {
			Brokers []string
			Topic   string
		}
	}
	KakaoMessaging struct {
		Account struct {
			Id       string
			Password string
		}
		AuthUrl                   string
		SendingUrl                string
		KakaoChannelSenderKey     string
		MessageSendingDomain      string
		MessageSendingAdminDomain string
	}
	JwtSecret string
}{}

// 서비스 무관하게 공통으로 사용하는 부분
func ConfigureEnvironment(path string, env ...string) {
	configor.Load(&Config, path+"config/config.json")
	properties := make(map[string]string)

	for _, key := range env {
		arg := os.Getenv(key)
		if len(arg) == 0 {
			panic(fmt.Errorf("No %s system env variable\n", key))
		}
		properties[key] = arg
	}

	afterPropertiesSet(properties)
}

// 서비스별 처리 로직이 달라지는 부분.
func afterPropertiesSet(properties map[string]string) {
	Config.KakaoMessaging.Account.Password = properties["KAKAO_MESSAGING_ACCOUNT_PASSWORD"]
	Config.JwtSecret = properties["JWT_SECRET"]
	if Config.JwtSecret == "" {
		Config.JwtSecret = "TEST"
	}
	Config.Encrypt.EncryptKey = properties["GOLANG_REST_API_ENCRYPT_KEY"]
	Config.AwsS3.SecretAccessKey = properties["GOLANG_REST_API_AWS_S3_KEY"]

	if properties["GOLANG_REST_API_DB_PASSWORD"] != "" {
		Config.Database.ConnectionString = fmt.Sprintf("%s:%s%s", Config.Database.User, properties["GOLANG_REST_API_DB_PASSWORD"], Config.Database.Connection)
	} else {
		Config.Database.ConnectionString = Config.Database.Connection
	}

	if properties["GOLANG_REST_API_DB_PASSWORD"] != "" {
		Config.MemberDatabase.ConnectionString = fmt.Sprintf("%s:%s%s", Config.Database.User, properties["GOLANG_REST_API_DB_PASSWORD"], Config.Database.Connection)
	} else {
		Config.MemberDatabase.ConnectionString = Config.MemberDatabase.Connection
	}
}

func ConfigureLogger() {
	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	logrus.SetOutput(os.Stdout)

	// Only log the warning severity or above.
	logrus.SetLevel(logrus.InfoLevel)
}
