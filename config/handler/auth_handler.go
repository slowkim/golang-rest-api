package handler

import (
	"context"
	"errors"
	"fmt"
	"golang-rest-api/common"
	"golang-rest-api/config"
	auth "golang-rest-api/config/authentication"
	"net/http"
	"strings"
	"time"

	"github.com/labstack/gommon/log"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
)

var (
	authenticator Authenticator
)

func CheckAuthMiddleware(allowRoles []string) echo.MiddlewareFunc {
	allowRoleMap := make(map[string]bool)
	for _, role := range allowRoles {
		allowRoleMap[role] = true
	}
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) (err error) {
			userClaim := common.GetUserClaim(c.Request().Context())

			if userClaim == nil {
				log.Warnf("No valid credentials: %s", c.Request().RequestURI)
				return c.JSON(http.StatusUnauthorized, "Please provide valid credentials")
			}

			if allowRoles[0] == "*" {
				return next(c)
			}

			roles := strings.Split(userClaim.Roles, ",")

			for _, role := range roles {
				if allowRoleMap[role] {
					return next(c)
				}
			}

			log.Warnf("Can't access this API: %s", c.Request().RequestURI)
			return c.JSON(http.StatusForbidden, "Can't access this API")
		}
	}
}

func InitUserClaimMiddleware() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) (err error) {
			token := c.Request().Header.Get("Authorization")
			if len(token) == 0 {
				if cookie, err := c.Cookie("token"); err != nil {
					return next(c)
				} else {
					token = cookie.Value
				}
			} else {
				index := strings.Index(token, "Bearer")
				if index < 0 {
					index = strings.Index(token, "Bearer")
				}
				if index >= 0 {
					token = token[index+len("Bearer"):]
					token = strings.Trim(token, " ")
				}
			}

			userClaim, _ := authenticator.getUserClaim(c.Path(), token)
			if userClaim == nil {
				return next(c)
			}

			req := c.Request()
			req = req.
				WithContext(context.WithValue(req.Context(), "token", token)).
				WithContext(context.WithValue(req.Context(), "userClaim", userClaim))
			c.SetRequest(req)
			return next(c)
		}
	}
}

type Authenticator struct {
	excepts map[string]bool
}

func (authenticator *Authenticator) getUserClaim(path string, token string) (*auth.UserClaim, error) {
	parsedToken, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) { return []byte(config.Config.JwtSecret), nil })

	if err != nil {
		return nil, errors.New("JWT parsing error: " + err.Error())
	}

	if jwt.SigningMethodHS256.Alg() != parsedToken.Header["alg"] {
		err := errors.New(fmt.Sprintf("Error: jwt token is expected %s signing method but token specified %s",
			jwt.SigningMethodHS256.Alg(), parsedToken.Header["alg"]))

		return nil, err
	}

	if !parsedToken.Valid {
		return nil, errors.New("Invalid JWT")
	}

	claimInfo, ok := parsedToken.Claims.(jwt.MapClaims)
	if !ok {
		return nil, errors.New("Cannot get jwt.MapClaims")
	}

	//check expire time
	if v, ok := claimInfo["exp"]; ok {
		expireTime := time.Unix(int64(v.(float64)), 0)
		if expireTime.Before(time.Now()) {
			log.Infof("JWT expired: %s, %s", claimInfo["id"], time.Now().Format(common.DateLayout19))

			return nil, errors.New("your session token was expired. please login")
		}
	}

	userClaim, err := auth.UserClaimFromMapClaim(claimInfo)
	if err != nil {
		return nil, err
	}
	userClaim.Token = token

	return userClaim, nil
}
