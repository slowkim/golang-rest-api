package config

import (
	val "golang-rest-api/dto/request/validator"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func ConfigureEcho() *echo.Echo {
	e := echo.New()
	e.Validator = RegisterValidator()
	e.HideBanner = true

	e.Pre(middleware.RemoveTrailingSlash())
	e.Use(middleware.Recover())
	e.Use(middleware.CORS())

	return e
}

func RegisterValidator() *CustomValidator {
	customValidator := validator.New()
	customValidator.RegisterValidation("businessRegistrationNo", val.ValidateBusinessRegistrationNo)
	customValidator.RegisterValidation("nativeRegistrationNo", val.ValidateNativeRegistrationNo)
	customValidator.RegisterValidation("foreignerRegistrationNo", val.ValidateForeignerRegistrationNo)
	customValidator.RegisterValidation("telNo", val.ValidateTelNo)
	customValidator.RegisterValidation("mobileNo", val.ValidateMobile)
	customValidator.RegisterValidation("postNo", val.ValidatePostNo)
	customValidator.RegisterValidation("date8", val.ValidateDate8)

	return &CustomValidator{validator: customValidator}
}

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) (err error) {
	return cv.validator.Struct(i)
}
