package auth

import (
	"errors"
	"golang-rest-api/config"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
)

type UserClaim struct {
	Token    string
	Id       int64
	Roles    string
	Nbf      int64
	Exp      int64
	Nickname string
	Aud      string
	Iss      string
}

func CreateToken(id int64, nickname string, roles ...string) (string, error) {
	expireTime := time.Now().Add(time.Hour * 24)
	claims := jwt.MapClaims{
		"id":       id,
		"nickname": nickname,
		"roles":    strings.Join(roles, ","),
		"iss":      "GOLANG_REST_API",
		"aud":      "GOLANG_REST_API",
		"nbf":      time.Now().Add(-time.Minute * 5).Unix(),
		"exp":      expireTime.Unix(),
	}

	return jwt.NewWithClaims(jwt.SigningMethodHS256, claims).
		SignedString([]byte(config.Config.JwtSecret))
}

func UserClaimFromMapClaim(claims jwt.MapClaims) (*UserClaim, error) {
	if claims == nil {
		return nil, errors.New("No jwt.MapClaims")
	}

	var (
		id, nbf, exp    int64
		aud, iss, roles string
	)

	if v, ok := claims["id"]; ok {
		id = int64(v.(float64))
	}
	if v, ok := claims["nbf"]; ok {
		nbf = int64(v.(float64))
	}
	if v, ok := claims["exp"]; ok {
		exp = int64(v.(float64))
	}
	if v, ok := claims["roles"]; ok {
		roles = v.(string)
	}
	if v, ok := claims["aud"]; ok {
		aud = v.(string)
	}
	if v, ok := claims["iss"]; ok {
		iss = v.(string)
	}

	return &UserClaim{
		Id:    id,
		Roles: roles,
		Nbf:   nbf,
		Exp:   exp,
		Aud:   aud,
		Iss:   iss,
	}, nil
}
